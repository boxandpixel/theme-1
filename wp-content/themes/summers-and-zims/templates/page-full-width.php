  <?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div id="page" role="main" class="home">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
      <header>
          <h1 class="entry-title"><?php the_title(); ?></h1>
      </header>
      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <div class="entry-content">
<?php
        if(have_rows("full_width")):
          while(have_rows("full_width")): the_row(); 
?>
      <div class="row boxes" data-equalizer data-equalize-on="large">          
<?php
            if(get_row_layout("cards")):

              if(have_rows("cards")):
                while(have_rows("cards")): the_row();
?>
        <div class="medium-4 columns">
          <a href="<?php the_sub_field('card_action'); ?>">
            <div class="home-box" data-equalizer-watch>
              <h3><?php the_sub_field("title"); ?></h3>
              <div class="card-core">
<?php
              if(get_sub_field("card_type") == "Image"):
                $card_image = get_sub_field("image");
?>
               <img src="<?php echo $card_image['url']; ?>"
                alt="<?php echo $card_image['alt']; ?>">
<?php
              elseif(get_sub_field("card_type") == "Video"):
?>
                <?php the_sub_field("video"); ?>
<?php
              elseif(get_sub_field("card_type") == "Content"):
?>
                <?php the_sub_field("content"); ?>               
<?php
              elseif(get_sub_field("card_type") == "Script"):
?>
                <?php the_sub_field("script"); ?>             
<?php
              endif;
?>
                <div class="card-text">
                  <?php the_sub_field("card_text"); ?>
                </div> <!-- card-action -->               
              </div> <!-- .card-core -->
            </div> <!-- .home-box -->
          </a>
        </div> <!-- columns -->            
<?php
                endwhile;
              endif;

            endif;

          endwhile;
        endif;
?>        
      <footer>
          <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
          <p><?php the_tags(); ?></p>
      </footer>
      <?php do_action( 'foundationpress_page_before_comments' ); ?>
      <?php comments_template(); ?>
      <?php do_action( 'foundationpress_page_after_comments' ); ?>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>
