<?php
/**
 * Template Name: Services Landing
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="service"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

 <!-- Services Page -->

	<div class="row">
    <div class="medium-6 large-8 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
      <?php the_content(); ?> 

<?php 
  if(have_rows('services_landing')):
    while(have_rows('services_landing')): the_row();
      if(get_row_layout() == 'service_landing_main'):
?>
      <div class="row">
<?php
        if(have_rows('service_landing')):
          while(have_rows('service_landing')): the_row();
?>
        <div class="large-6 columns">
          <div class="service-box">
            <h3><?php the_sub_field("service_title"); ?></h3>
        <?php 
                    if(get_sub_field('service_image') != ""):
                      
                      // Box Image: Get and display from array
                $service_image = get_sub_field('service_image');
                $service_image_url = $service_image['url'];
                $service_image_alt = $service_image['alt'];
                $service_image_small = $service_image['sizes']['small'];
                $service_image_medium = $service_image['sizes']['medium'];
                $service_image_large = $service_image['sizes']['large'];
                $service_image_xlarge = $service_image['sizes']['x-large'];             
                
        ?> 
            <a href="<?php the_sub_field('service_link'); ?>">
              <img
                src="<?php echo $service_image_small; ?>"
                srcset="<?php echo $service_image_medium; ?> 360w, 
                        <?php echo $service_image_large; ?> 570w,
                        <?php echo $service_image_xlarge; ?> 740w"
                sizes="(min-width: 770px) 370px, 740px"

                alt="<?php echo $service_image_alt; ?>"
              >              
            </a>
        <?php
                    endif;
                    the_sub_field("service_intro");
        ?>
            <a href="<?php the_sub_field('service_link'); ?>" class="button blue"><?php the_sub_field('service_link_text'); ?></a>      
          </div> <!-- .service-box -->
        </div> <!-- .large-6 -->
<?php
          endwhile; // while(have_rows('service_landing'))
        endif; // if(have_rows('service_landing'))
?>            
      </div> <!-- .row -->
<?php
      endif; // if(get_row_layout() == 'service_landing_main'):
    endwhile; // while(have_rows('services_landing')): the_row();
  endif; // if(have_rows('services_landing')):
?>      
    </div> <!-- .medium-6 large-8 -->
    <div class="medium-6 large-4 columns">
      <div class="service-sidebar">
<?php
  if(have_rows('sidebar_content')):
    while(have_rows('sidebar_content')): the_row();
?>

<?php
    if(get_row_layout() == "sidebar_review"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_review_title")):
              echo "<h3>" . get_sub_field("sidebar_review_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_review")):
              the_sub_field("sidebar_review");
            endif;

            if(get_sub_field("sidebar_review_link")):
              echo "<a href='".get_sub_field("sidebar_review_link")."' class='button blue'>" . get_sub_field("sidebar_review_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->  

<?php
    elseif(get_row_layout() == "sidebar_membership"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_membership_title")):
              echo "<h3>" . get_sub_field("sidebar_membership_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_membership_description")):
              the_sub_field("sidebar_membership_description");
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box --> 

<?php
    elseif(get_row_layout() == "sidebar_posts"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_posts_title")):
              echo "<h3>" . get_sub_field("sidebar_posts_title") . "</h3>";
            endif;

            if(have_rows("sidebar_posts")):
              while(have_rows("sidebar_posts")): the_row();

                $posts = get_sub_field('sidebar_post');

                if($posts):
?>
          <article class="service-posts">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <footer>
              Posted on <?php the_date(); ?> in <?php the_category(); ?>
            </footer>
            <?php endforeach; ?>
          </article>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php 
                endif;                
              endwhile;
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>
        </div> <!-- .box -->         

<?php
    elseif(get_row_layout() == "sidebar_appointment"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_appointment_title")):
              echo "<h3>" . get_sub_field("sidebar_appointment_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_appointment_description")):
              the_sub_field("sidebar_appointment_description");
            endif;

            if(get_sub_field("sidebar_appointment_link")):
              echo "<a href='".get_sub_field("sidebar_appointment_link")."' class='button blue'>" . get_sub_field("sidebar_appointment_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->                     



<?php
    elseif(get_row_layout() == "sidebar_image"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_image_title")):
              echo "<h3>" . get_sub_field("sidebar_image_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_image_description")):
              the_sub_field("sidebar_image_description");
            endif;

            if(get_sub_field("sidebar_image")):
              $sidebar_image = get_sub_field("sidebar_image");
              $sidebar_image_url = $sidebar_image['url'];
              $sidebar_image_alt = $sidebar_image['alt'];
            endif;

            if(get_sub_field("sidebar_image") && get_sub_field("sidebar_image_link")):
              echo "<a href='".get_sub_field("sidebar_image_link")."'>";
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
              echo "</a>";
            endif;

            if(get_sub_field("sidebar_image") && !get_sub_field("sidebar_image_link")):
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
            endif;

?>

        </div> <!-- .box -->   

<?php
    elseif(get_row_layout() == "sidebar_script"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_script_title")):
              echo "<h3>" . get_sub_field("sidebar_script_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_script_description")):
              the_sub_field("sidebar_script_description");
            endif;

            if(get_sub_field("sidebar_script")):
              echo the_sub_field("sidebar_script"); 
            endif;            
?>

        </div> <!-- .box -->        
<?php
    endif; // end get_row_layouts
?>
       
<?php
    endwhile;
  endif;
?>
      </div> <!-- .service-sidebar -->
    </div> <!-- medium-6 large-4 sidebar -->
	</div> <!-- .row -->

  
 <!-- end page -->


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div> <!-- #page -->

 <?php get_footer(); ?>
