<?php
/**
 * Template Name: Contact Joe
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="contact-joe"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

  <div class="row">
    <div class="medium-5 columns">
<?php
        if(get_field('joe_image') != ""):
          $image = get_field('joe_image');
          $image_alt = $image['alt'];
          $image_small = $image['sizes']['small'];
          $image_medium = $image['sizes']['medium'];
          $image_large = $image['sizes']['large'];
          $image_xlarge = $image['sizes']['x-large'];

?>
          <img
            src="<?php echo $image_small; ?>"
            srcset="<?php echo $image_medium; ?> 360w, 
                    <?php echo $image_large; ?> 570w,
                    <?php echo $image_xlarge; ?> 740w"
            sizes="(min-width: 770px) 570px, 570px"

            alt="<?php echo $image_alt; ?>"
          >

<?php
        endif; // if(get_field('joe_image')
?>
    <?php the_field('joe_image_caption'); ?>       

    </div> <!-- .medium-5 -->
    <div class="medium-7 columns">
      <?php the_field('joe_form'); ?>
    </div> <!-- .medium-7 -->
  </div> <!-- .row -->

<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
