<?php
/**
 * Template Name: Staff
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="staff"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>      
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->


 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        'offset' => 0,
        'post_type' => 'staff', 
        'orderby' => 'title',
        'order' => 'asc'
      )); 
      if( $posts ):
?>

  <div class="row">

<?php
      foreach( $posts as $post ):     
        setup_postdata($post);

        $thumb = get_field("thumbnail_image");
        $thumb_alt = $thumb['alt'];
        $thumb_180 = $thumb['sizes']['thumbnail'];
        $thumb_320 = $thumb['sizes']['small']; 
        $thumb_480 = $thumb['sizes']['medium'];        
             
?>
    <div class="small-12 medium-4 large-3 columns">
      <div class="staff-each">
        <a href="<?php the_permalink(); ?>"></a>
          <img 
            src="<?php echo $thumb_320; ?>" 
            srcset="<?php echo $thumb_480; ?> 480w"
            sizes="(max-width: 375px) 100vw, (min-width: 48em) 480px, 480px"
            
            alt="<?php echo $thumb_alt; ?>"
          >  
        <div class="staff-name-position">
          <span class="staff-name"><?php the_field("name"); ?></span>
          <span class="staff-position"><?php the_field("job_title"); ?></span>
        </div> <!-- .staff-name-position -->
      </div> <!-- .staff-each -->      
    </div> <!-- .small-6 -->    

<?php
        wp_reset_postdata();
      endforeach;
?>
  </div> <!-- .row -->

<?php endif; ?>

<?php endwhile;?>


 </div>

 <?php get_footer(); ?>
