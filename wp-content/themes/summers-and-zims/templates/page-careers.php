  <?php
/**
 * Template Name: Careers
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="general careers">  

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 

   <div class="row">
    <div class="small-12 columns">
     
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>      
      <?php the_content(); ?>

    </div> <!-- .small-12 -->
  </div> <!-- .row -->


 <!-- Careers Page -->
  <div class="mission">
    <div class="row">
      <div class="small-12 collapse">
  <?php 
          $careers_image = get_field("image");
          $careers_image_url = $careers_image['url'];
  ?>      
        <div class="mission-image" style="background: url('<?php echo $careers_image_url; ?>') center bottom no-repeat; background-size: cover;">
          <div class="row">
            <div class="small-12 medium-8 medium-centered columns">
              <div class="mission-statement">
                <h3><?php the_field("mission"); ?></h3>
              </div> <!-- .mission-statement -->
            </div> <!-- .columns -->
          </div> <!-- .row -->
        </div> <!-- background image -->
      </div> <!-- .small-12 -->
    </div> <!-- .row -->
  </div> <!-- .mission -->

  <div class="video">
    <div class="row">
      <div class="small-12 medium-6 medium-centered columns">
        <?php the_field("careers_video"); ?>
      </div>
    </div>
  </div>

  <div class="benefits">
    <div class="row">
      <div class="small-12 medium-8 medium-centered columns">
        <?php the_field("benefits"); ?>
      </div> <!-- .columns -->
    </div> <!-- .row -->
  </div> <!-- .benefits -->

  <div class="positions">
    <div class="row">
      <div class="small-12 medium-8 medium-centered columns">
        <?php the_field("positions_intro"); ?>

<?php
        if(have_rows("positions")):
?>
        <ul class="accordion" data-accordion data-allow-all-closed="true"> 
<?php                                
          while(have_rows("positions")): the_row();
?>
          <li class="accordion-item" data-accordion-item>
            <a href="#" class="accordion-title">
              <?php the_sub_field("position_title"); ?>
            </a>
            <div class="accordion-content" data-tab-content><?php the_sub_field("position"); ?></div>
          </li> <!-- .position -->            
<?php
          endwhile;
?>
        </ul>
<?php                  
        endif;
?>                  
      </div> <!-- .columns -->
    </div> <!-- .row -->
  </div> <!-- .positions -->

  <div class="application">
    <div class="row">
      <div class="small-12 medium-8 medium-centered columns">
        <h2><?php the_field("application_form_title"); ?></h2>
        <div class="application-form">
          <?php the_field("application_form"); ?>
        </div> <!-- .application-form -->
      </div> <!-- .columns -->
    </div> <!-- .row -->
  </div> <!-- .application -->

<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
