<?php
/**
 * Template Name: Home Update
 *
 * Updated home page template
 */

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<title><?php wp_title( '|' ); ?></title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php wp_head(); ?>
</head>
<body class="update">
	


	<header id="update">

		<div class="header__time-message">
			<?php get_template_part( 'parts/time-message' ); ?>
			<span class="call-message"><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone_message", "options"); ?></a></span>
			<span class="call-number"><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone_number", "options"); ?></a></span>
		</div>

		
		<div class="header__left">
			<div class="header__logo">
			<?php 
				$logo = get_field("logo_update", "options");
				$logo_small = get_field("logo_small", "options");
			?>
				<img src="<?php echo $logo['url'] ?>" id="logo">
				<img src="<?php echo $logo_small['url'] ?>" id="logo-small">
			</div>
		</div>

		<div class="header__right">

			<?php wp_nav_menu( 
				array( 

					'theme_location' => 'nav-menu',
					'container' => '',
					'items_wrap' => '<ul class="header__main-menu">%3$s</ul>',
					'menu_class' => ''
				)
			); ?>
			
			<?php
				$years = get_field("years_of_experience", "options");
				$years_url = $years['url'];
				$years_alt = $years['alt'];

				// Get Years in Business
				function getYears($then) {
				    $then_ts = strtotime($then);
				    $then_year = date('Y', $then_ts);
				    $years = date('Y') - $then_year;
				    if(strtotime('+' . $years . ' years', $then_ts) > time()) $years--;
				    return $years;
				}
			?>
			<div class="header__years-of-experience">
				<div class="header__years--show">
				    <span class="header__years"><?php print getYears('1930-01-01');  ?></span>
				    <img src="<?php echo $years_url; ?>" alt="<?php echo $years_alt; ?>">
				    <span class="header__years-duration">1930 - <?php echo date('Y'); ?></span>
			  </div>
			</div>
		</div>
	</header>

	<main>
	 <?php while ( have_posts() ) : the_post(); ?>
		

	 	<div class="section__hero">
	 		<?php 
	 			$hero_image = get_field("hero_image");
	 			$hero_video = get_field("hero_video");	
	 		?>

	 		<?php if($hero_image): ?>
	 		<img src="<?php echo $hero_image['url']; ?>">
		 	<?php endif; ?>

			<?php if($hero_video): ?>
	 		<video class="hero-video" preload="auto" autoplay="" playsinline="" loop="" muted="" poster="">
	 			<source src="<?php echo $hero_video['url']; ?>">
	 		</video>
		 	<?php endif; ?>
	 	</div>		

		<div class="section__featured-cards">
			<?php
			if(have_rows('featured_cards')):
			  while(have_rows('featured_cards')): the_row();

		  	?>
			<section class="featured-card__card">
				<?php $featured_image = get_sub_field("featured_card_image"); ?>
				<figure>
					<img src="<?php echo $featured_image['url']; ?>" alt="">
				</figure>
				<main>
					<h3><?php the_sub_field("featured_card_heading"); ?></h3>
					<p><?php the_sub_field("featured_card_description"); ?></p>
					<?php $featured_link = get_sub_field("featured_card_link"); ?>
					<a href="<?php echo $featured_link['url']; ?>" class="button blue"><?php echo $featured_link['title']; ?></a>
				</main>
			</section>
			<?php 
				endwhile;
			endif;
			?>
		</div>

		<div class="section__staff">
			<h2><?php the_field("staff_heading"); ?></h2>
			<ul class="staff__staff-members">
			<?php


			  $post_objects = get_field('staff_members');
			  if($post_objects):
			    foreach ($post_objects as $post):
			    setup_postdata($post);

			    $staff_member_image = get_field("thumbnail_image");
			?>
				
					<li>
						<figure>
							<img src="<?php echo $staff_member_image['url']; ?>" alt="">
							<figcaption>
								<h4><?php the_field("name"); ?></h4>
								<?php the_field("job_title"); ?>
							</figcaption>
						</figure>
					</li>
				
			<?php


                wp_reset_postdata();
              	endforeach;
          	endif;
			?>
			</ul>
			<?php
				$staff_page_link = get_field("staff_page_link");
			?>

			<a href="<?php echo $staff_page_link['url']; ?>" class="button blue"><?php echo $staff_page_link['title']; ?></a>                
		</div>	

		<div class="section__the-difference">
			<h2><?php the_field("the_difference_title"); ?></h2>

			<div class="the-difference__featured">
				<ul>
				<?php
					if(have_rows("the_difference_tabs")):
						while(have_rows("the_difference_tabs")): the_row();
				?>
					<li class="difference__featured-<?php echo get_row_index(); ?>" style="background-color: <?php the_sub_field('difference_color'); ?> ;">
						<h4><?php the_sub_field("difference_title"); ?></h4>
						<?php the_sub_field("difference_description"); ?>	
					</li>
				<?php 
						endwhile;
					endif;
				?>
				</ul>
			</div>
			<div class="the-difference__pagination">
				<ul>
					<?php
						if(have_rows("the_difference_tabs")):
							while(have_rows("the_difference_tabs")): the_row();
					?>
						<li class="difference__paginate-<?php echo get_row_index(); ?>" style="background-color: <?php the_sub_field('difference_color'); ?> ;"><?php the_sub_field("difference_title"); ?></li>
					<?php 
							endwhile;
						endif;
					?>
				</ul>
			</div>
		</div>

		<div class="section__home-performance">
			<h2><?php the_field("home_performance_title"); ?></h2>
			<?php
			if(have_rows('home_performance_cards')):
			  while(have_rows('home_performance_cards')): the_row();

		  	?>


			<section class="featured-card__card">
				<?php $featured_image = get_sub_field("performance_service_image"); ?>
				<figure>
					<img src="<?php echo $featured_image['url']; ?>" alt="">
				</figure>
				<main>
					<h3><?php the_sub_field("performance_service_heading"); ?></h3>
					<p><?php the_sub_field("performance_service_description"); ?></p>
					<?php $performance_service_link = get_sub_field("performance_service_link"); ?>
					<a href="<?php echo $performance_service_link['url']; ?>" class="button blue"><?php echo $performance_service_link['title']; ?></a>
				</main>
			</section>

			<?php 
				endwhile;
			endif;
			?>	
		</div>

		<div class="section__reviews">
			
			<section class="review">
				<?php 
				  $reviews = get_field('review');
				  if($reviews):
				    $post = $reviews;
				    setup_postdata($post);	    
				?>
				<blockquote>
					<q><?php the_field("review_quote"); ?></q>
					<span class="author"><?php the_field("review_author"); ?></span> - <span class="location"><?php the_field("review_location"); ?></span> 
				</blockquote>
				<?php
	                wp_reset_postdata();
	              	
	          	endif;
				?>	
			</section>
		</div>

		<div class="section__brands">
			
			<h2><?php the_field("brands_heading"); ?></h2>
			<?php the_field("brands_description"); ?>

			<ul class="brands">
			<?php 
				if(have_rows("brands_list")):
					while(have_rows("brands_list")): the_row();

						$brands_image = get_sub_field("brand_image");
			?>
				<li>
					<figure>
						<img src="<?php echo $brands_image['url']; ?>" alt="">
					</figure>
				</li>
			<?php						
					endwhile;
				endif;
			?>
			</ul>
		</div>
	 <?php endwhile;?>
	</main>

	<footer>
		

		<section class="footer__left">
			<p>&copy; Copyright <?php echo date("Y"); ?>, Summers &amp; Zim's. All Rights Reserved.</p>
		<?php
	        if(have_rows("footer_left", "options")):

	            while(have_rows("footer_left", "options")): the_row();

	                if(get_row_layout() == "visual_editor_section"):
		?>
            <div class="section">
                <?php the_sub_field("custom_visual_editor", "options"); ?>
            </div> <!-- .footer-section -->
		<?php
                    elseif(get_row_layout() == "plain_text_section"):
		?>                                
            <div class="footer-section">
                <p><?php the_sub_field("custom_plain_text", "options"); ?></p>
            </div>
		<?php                         
                    endif;

                endwhile;
            endif;
		?>
		</section>
		<section class="footer__right">
		<?php
            if(have_rows("footer_right", "options")):

                while(have_rows("footer_right", "options")): the_row();
  
                    if(get_row_layout() == "visual_editor_section"):
		?>
            <div class="section">
                <?php the_sub_field("custom_visual_editor", "options"); ?>
            </div> <!-- .footer-section -->
		<?php
                    elseif(get_row_layout() == "plain_text_section"):
		?>                                
            <div class="section">
                <?php the_sub_field("custom_plain_text", "options"); ?>
            </div>
		<?php
	                elseif(get_row_layout() == "map_section"):
		?>                                
            <div class="section">
                <div id="footer-map">
                    <?php //the_sub_field("map_field", "options"); ?>
                    <div id="map"></div>
                    <script>
                        
                        function initMap() {
                            var sumzim = {lat: 39.949127, lng: -75.974558};
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: 39.949700, lng: -75.974558},
                                zoom: 17,
                                scrollwheel: false
                            });
                            var marker = new google.maps.Marker({
                                position: sumzim,
                                map: map
                            }); 

                            var loc = "<h3 style='font-size: 14px; color: black; font-weight: bold; margin: 0;'><?php the_sub_field('map_location', 'options'); ?></h3>";
                            var street = "<p style='color: #333; margin: 0;'><?php the_sub_field('map_address_1', 'options') ?></p>";
                            var csz = "<p style='color: #333; margin: 0;'><?php the_sub_field('map_address_2', 'options') ?></p>";
                            var link = "<a href='<?php the_sub_field('map_link_url', 'options') ?>' target='_blank'><?php the_sub_field('map_link_text', 'options'); ?></a>";
                            var infowindow = new google.maps.InfoWindow({

                                content: loc + street + csz + link
                            });

                            infowindow.open(map, marker);                                                                           
                        }
                    </script>                                
                </div> <!-- #footer-map -->
            </div> 
		<?php
                    elseif(get_row_layout() == "social_icon_section"):
		?>                                

		                            
		<?php 
                    if(have_rows("social_icons", "options")):
		?>
                        <ul class="social">
		<?php                                                            
                        while(have_rows("social_icons", "options")): the_row();

                        $social_image = get_sub_field("social_icon");
                        $social_image_url = $social_image['url'];
                        $social_image_alt = $social_image['alt'];
		?>
	                        <li>
	                            <a href="<?php the_sub_field('social_url'); ?>" target="_blank"><img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_url; ?>"></a>
	                        </li>
		<?php                                 
                        endwhile;
		?>                       
                        </ul>
        <?php
                    endif;
		?>
		                            
                                              
		<?php                         
                    endif;

                endwhile;
            endif;
		?>	
		</section>

		<script>
		    function loadMaps() {
		        if (typeof initMap == 'function') { 
		          initMap();
		        }
		        if (typeof initServiceMap == 'function') { 
		          initServiceMap();
		        }   
		    }
		</script>

		<!-- Google Maps -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEgEBKltqyDUZPYxhSgHsNARDUXBQ0f2I&callback=loadMaps"
		    async defer></script>
		<?php wp_footer(); ?>
	</footer>

</body>
</html>


