<?php
/**
 * Template Name: Home Performance Product
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="home-performance-products"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


<?php 

  if(have_rows("home_performance_products")):
    while(have_rows("home_performance_products")): the_row();

      if(get_row_layout() == "product_logo"):
?>
      <div class="row product-logo">
        <div class="medium-12 columns end">
<?php
            $image = get_sub_field("product_logo");
            $image_url = $image['url'];
            $image_alt = $image['alt'];
            $image_180 = $image['sizes']['thumbnail'];
            $image_320 = $image['sizes']['small'];            

?>
          <img 
            src="<?php echo $image_url; ?>" 
            alt="<?php echo $image_alt; ?>"
          >           
          <?php dimox_breadcrumbs(); ?>
          <?php the_sub_field("product_description"); ?>
        </div> <!-- .medium-12 -->
      </div> <!-- .row -->
<?php
      elseif(get_row_layout() == "product_boxes"):
?>
      <div class="row product-boxes" data-equalizer data-equalize-on="medium">
<?php
        if(have_rows("product_box")):
          while(have_rows("product_box")): the_row();
?>
        <div class="medium-4 columns">
          <div class="box" data-equalizer-watch>
            <h3><?php the_sub_field("product_box_heading"); ?></h3>
            <?php the_sub_field("product_box_description"); ?>
          </div> <!-- .box -->  
        </div> <!-- .medium-4 -->
<?php
          endwhile;
        endif;
?>
      </div> <!-- .row -->
<?php
      elseif(get_row_layout() == "product_embed"):
?>
      <div class="row product-embed">
        <div class="medium-8 medium-centered columns">
          <h2><?php the_sub_field("embed_title"); ?></h2>
          <?php the_sub_field("embed_description"); ?>
          <div class="embed">
            <?php the_sub_field("embed_link"); ?>
          </div> <!-- .embed -->
        </div> <!-- .medium-4 -->

      </div> <!-- .row -->      
<?php
      elseif(get_row_layout() == "product_process"):
?>
      <div class="row product-process">
        <div class="medium-12 large-7 columns">
          <h2><?php the_sub_field("process_heading"); ?></h2>
          <?php the_sub_field("process_description"); ?> 
        </div> <!-- .medium-6 -->
        <div class="medium-12 large-5 columns">
<?php
            $process_image = get_sub_field("process_image");
            $process_image_url = $process_image['url'];
            $process_image_alt = $process_image['alt'];
            $process_image_320 = $process_image['sizes']['small'];
            $process_image_480 = $process_image['sizes']['medium'];            
?>
          <img 
            src="<?php echo $process_image_480; ?>" 
            srcset="<?php echo $process_image_320; ?> 320w, <?php echo $process_image_480; ?> 480w"
            sizes="(max-width: 375px) 100vw, (max-width: 640px) 640px, (min-width: 48em) 480px, 640px"
            
            alt="<?php echo $process_image_alt; ?>"
          >        
        </div> <!-- .medium-6 --> 
      </div> <!-- .row -->
<?php
        if(have_rows("process_steps")):
?>
        <div class="row process-steps" data-equalizer data-equalize-on="medium">
<?php
          while(have_rows("process_steps")): the_row();
?>
          <div class="small-6 medium-3 columns">
            <div class="box" data-equalizer-watch>
<?php
            $steps_image = get_sub_field("process_steps_image");
            $steps_image_url = $steps_image['url'];
            $steps_image_alt = $steps_image['alt'];
            $steps_image_320 = $steps_image['sizes']['small'];            
?>
              <img src="<?php echo $steps_image_320; ?>" alt="<?php echo $image_alt; ?>"> 
              <?php the_sub_field("process_steps_description"); ?>             
            </div> <!-- .box -->           
          </div> <!-- .medium-3 -->
<?php
          endwhile;
?> 
        </div> <!-- .row -->
<?php
        endif;
?>
<?php
      elseif(get_row_layout() == "product_inquiry"):
?>
      <div class="row product-inquiry">
        <div class="medium-12 columns">
          <?php the_sub_field("inquiry_text"); ?>
          <a href="<?php the_sub_field('inquiry_link'); ?>" class="button blue"><?php the_sub_field("inquiry_link_text"); ?></a>
        </div> <!-- .medium-12 -->
      </div> <!-- .row -->

<?php
      endif; // get_row_layout()
    endwhile;
  endif;
?>


<?php endwhile;?>


 </div>

 <?php get_footer(); ?>
