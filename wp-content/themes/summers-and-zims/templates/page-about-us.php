<?php
/**
 * Template Name: About Us
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="about"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 

  <div class="row">
    <div class="small-12 columns">
     
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>      
      <?php the_content(); ?>

    </div> <!-- .small-12 -->
  </div> <!-- .row -->
<?php 

  if(get_field('about_page')):       
    while(has_sub_field("about_page")):
?>

<?php      
      // Display main layout
      if(get_row_layout() == 'box_row'):

?>
  <div class="row boxes" data-equalizer data-equalize-on="medium">
<?php
        if(have_rows('box')):
          while(have_rows('box')): the_row();
?>    
    <div class="medium-4 columns">
      <div class="about-box" data-equalizer-watch>

      <h3><?php the_sub_field('box_title');?></h3>
<?php 
            if(get_sub_field('box_image')):
          // Box Image: Get and display from array
          $box_image = get_sub_field('box_image');
          $box_image_alt = $box_image['alt'];
          $box_image_small = $box_image['sizes']['small'];
          $box_image_medium = $box_image['sizes']['medium'];
          $box_image_large = $box_image['sizes']['large'];
          $box_image_xlarge = $box_image['sizes']['x-large'];    
        
?>
                  
            <a href="<?php the_sub_field('box_link'); ?>">
              <img
                src="<?php echo $box_image_small; ?>"
                srcset="<?php echo $box_image_medium; ?> 360w, 
                        <?php echo $box_image_large; ?> 570w,
                        <?php echo $box_image_xlarge; ?> 740w"
                sizes="(min-width: 770px) 370px, 740px"

                alt="<?php echo $box_image_alt; ?>"
              >
            </a>
<?php
            endif;
?>
<?php
            if(get_sub_field('box_video')):
?>
              <div class="box-video">
<?php
              the_sub_field('box_video');
?>
              </div> <!-- .box-video -->
<?php
            endif;
?>
<?php
            if(get_sub_field('box_content')):
?>
              <div class="box-content">
<?php
              the_sub_field('box_content');
?>
              </div> <!-- .box-content -->
<?php
            endif;
?>
      <div class="box-bottom">
        <?php the_sub_field('box_description'); ?>
<?php
        if(get_sub_field('box_link')):
?>
        <a href="<?php the_sub_field('box_link'); ?>" class="button blue"><?php the_sub_field('box_link_text'); ?></a>
<?php
        endif;  
?>
       
      </div> <!-- .box-bottom -->
    </div> <!-- .about-box -->
  </div> <!-- .medium-4 columns -->

<?php      
          endwhile; // while(have_rows('box'))
        endif; // if(have_rows('box'))
      //endif; // box_row
    
?>
  </div> <!-- .row .boxes -->

<?php
      // Display main layout
      elseif(get_row_layout() == 'audio_box'):
?>
  <div class="row audio">
    <div class="small-12 columns">
      <h3><?php the_sub_field('audio_title'); ?></h3>
      <?php the_sub_field('audio_description'); ?>
    </div> <!-- .small-12 --> 
  </div> <!-- .row audio -->

<?php
      elseif(get_row_layout() == 'affiliations_layout'):
?>
  <div class="row boxes affiliations" data-equalizer data-equalize-on="medium">
    
<?php
        if(have_rows('affiliation')):
          while(have_rows('affiliation')): the_row();
?>
    <div class="small-12 medium-6 columns">
      <div class="about-box" data-equalizer-watch>
        <h3><?php the_sub_field('affiliation_title'); ?></h3>
        <div class="box-content">
          <?php the_sub_field('affiliation_text'); ?>
        </div> <!-- .box-content -->
      </div> <!-- .about-box -->
    </div><!-- .small-12 medium-6 -->

<?php
          endwhile;
        endif;
?>  
  </div> <!-- .row affiliations -->
<?php
      endif; //get_row_layout() == x
?>

<?php 
    endwhile; // while(have_rows('service_page'))
  endif; // if(have_rows('about_page'))
?>

<?php endwhile;?>    


 <?php get_footer(); ?>
