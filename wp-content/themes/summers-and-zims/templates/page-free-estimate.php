<?php
/**
 * Template Name: Free Estimate
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="about"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 

  <div class="row">
    <div class="small-12 columns">
     
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>      

    </div> <!-- .small-12 -->
  </div> <!-- .row -->

  <div class="row">
    <div class="small-12 columns">
      <div class="row">
        <div class="small-12 medium-6 medium-push-6 columns">
<?php    
  if(get_field('general_page')):     
    while(has_sub_field("general_page")):
      if(get_row_layout() == "main_content"):
?>       
<?php
        the_sub_field('text');
        if(have_rows('images')):      
?>
          <div class="row">
<?php
          while(have_rows('images')): the_row();
            if(get_sub_field('image') != ""):
              $image = get_sub_field('image');
              $image_url = $image['url'];
              $image_alt = $image['alt'];
?>
            <div class="large-6 columns service-images">
              <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
            </div> <!-- .large-6 -->
<?php
            endif; // if(get_sub_field('service_image')
          endwhile; // while(have_rows('service_images'))
?>
          </div> <!-- .row -->
<?php 
        endif; //have_rows('images')
?>  
<?php
      endif; // if(get_row_layout() == "main_content")
    endwhile; // while(has_sub_field('general_page'))
  endif; // if(get_field('general_page')): 
?>
        </div> <!-- .columns -->
        <div class="small-12 medium-6 medium-pull-6 columns">
          <?php the_content(); ?>
        </div> <!-- .columns -->                
      </div> <!-- .row -->
    </div> <!-- .columns -->
  </div> <!-- .row -->


<?php endwhile;?>    


 <?php get_footer(); ?>
