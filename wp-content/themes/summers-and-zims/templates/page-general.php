<?php
/**
 * Template Name: General Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="general">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>


 <!-- General Page Content -->

  <div class="row">

    <div class="small-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>

      <!-- Start the page layout -->
      <div class="row">
<?php
  if(have_rows('general_page')):
?>
        <!-- Begin main content areas -->
        <div class="medium-6 large-8 columns">
<?php
    while(have_rows("general_page")): the_row();
      if(get_row_layout() == "main_content"):

        the_sub_field('text');
        if(have_rows('images')):
?>
          <div class="row">
<?php
          while(have_rows('images')): the_row();
            if(get_sub_field('image') != ""):
              $image = get_sub_field('image');
              $image_url = $image['url'];
              $image_alt = $image['alt'];
?>
            <div class="large-6 columns service-images">
                <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
            </div> <!-- .large-6 -->
<?php
            endif; // if(get_sub_field('service_image')
          endwhile; // while(have_rows('service_images'))
?>
          </div> <!-- .row -->
<?php
        endif; //have_rows('images')


      elseif(get_row_layout() == "image_group"):
        $image_group_image = get_sub_field("image")['url'];
        $image_group_image_alt = get_sub_field("image")['url'];
?>
        <picture>
          <img src="<?php echo $image_group_image; ?>" alt="<?php echo $image_group_image_alt; ?>">
        </picture>

<?php
      elseif(get_row_layout() == "content_grouping"):
        $link_group = get_sub_field("link_group_type");
?>
      <div class="link-group">
        <span class="jump">Jump to Section</span>
<?php
          if($link_group == "Numbers"): ?>
        <ol>
<?php
          elseif($link_group == "Bullets"): ?>
        <ul>
<?php
          endif; ?>
<?php
          if(have_rows("content_group")):
            while(have_rows("content_group")): the_row();
?>
            <li><a href="#<?php the_sub_field('link_reference'); ?>"><?php the_sub_field("section_title"); ?></a></li>
<?php
            endwhile;
          endif;
?>

<?php
          if($link_group == "Numbers"): ?>
        </ol>
<?php
          elseif($link_group == "Bullets"): ?>
        </ul>
<?php
          endif;
?>
      </div> <!-- .link-group -->
      <div class="content-group">
<?php
          if(have_rows("content_group")):

            if($link_group == "Numbers"): ?>
        <ol>
<?php
            elseif($link_group == "Bullets"): ?>
        <ul>
<?php
            endif;
            while(have_rows("content_group")): the_row();
?>
            <li class="content-group-each" id="<?php the_sub_field('link_reference'); ?>">

              <h3><?php the_sub_field("section_title"); ?></h3>
<?php
              if(have_rows("section_content_group")):
                while(have_rows("section_content_group")): the_row();



                  if(get_row_layout() == "section_content"):
                    the_sub_field("content");
                  elseif(get_row_layout() == "section_images"):
                    $content_group_image = get_sub_field("image")['url'];

                    $content_group_image_alt = get_sub_field("image")['alt'];
?>
              <picture>
                <img src="<?php echo $content_group_image; ?>" alt="<?php echo $content_group_image_alt; ?>">
              </picture>
<?php
                  endif;

                endwhile;
              endif; ?>


            </li> <!-- .content-group-each -->
<?php
            endwhile;
            if($link_group == "Numbers"): ?>
        </ol>
<?php
            elseif($link_group == "Bullets"): ?>
        </ul>
<?php
            endif;
          endif;

?>
      </div> <!-- .content-group -->
<?php

      endif; // if(get_row_layout())


    endwhile; // while(have_rows('general_page'))
?>
</div> <!-- .medium-6 -->
<?php
  endif; // if(have_rows('general_page')):
?>

<?php
  if(have_rows('sidebar_content')):
    while(have_rows('sidebar_content')): the_row();
?>
        <div class="medium-6 large-4 columns">
          <div class="page-sidebar">
<?php
    if(get_row_layout() == "sidebar_review"):
?>
            <div class="box">
<?php
            if(get_sub_field("sidebar_review_title")):
              echo "<h3>" . get_sub_field("sidebar_review_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_review")):
              the_sub_field("sidebar_review");
            endif;

            if(get_sub_field("sidebar_review_link")):
              echo "<a href='".get_sub_field("sidebar_review_link")."' class='button blue'>" . get_sub_field("sidebar_review_link_text") . "</a>";
            endif;
?>

            </div> <!-- .box -->

<?php
    elseif(get_row_layout() == "sidebar_membership"):
?>
            <div class="box">
<?php
            if(get_sub_field("sidebar_membership_title")):
              echo "<h3>" . get_sub_field("sidebar_membership_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_membership_description")):
              the_sub_field("sidebar_membership_description");
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;
?>

            </div> <!-- .box -->

<?php
    elseif(get_row_layout() == "sidebar_posts"):
?>
            <div class="box">
<?php
            if(get_sub_field("sidebar_posts_title")):
              echo "<h3>" . get_sub_field("sidebar_posts_title") . "</h3>";
            endif;

            if(have_rows("sidebar_posts")):
              while(have_rows("sidebar_posts")): the_row();

                $posts = get_sub_field('sidebar_post');

                if($posts):
?>
              <article class="service-posts">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <footer>
              Posted on <?php the_date(); ?> in <?php the_category(); ?>
                </footer>
            <?php endforeach; ?>
              </article>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php
                endif;
              endwhile;
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;
?>
            </div> <!-- .box -->

<?php
    elseif(get_row_layout() == "sidebar_appointment"):
?>
            <div class="box">
<?php
            if(get_sub_field("sidebar_appointment_title")):
              echo "<h3>" . get_sub_field("sidebar_appointment_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_appointment_description")):
              the_sub_field("sidebar_appointment_description");
            endif;

            if(get_sub_field("sidebar_appointment_link")):
              echo "<a href='".get_sub_field("sidebar_appointment_link")."' class='button blue'>" . get_sub_field("sidebar_appointment_link_text") . "</a>";
            endif;
?>

            </div> <!-- .box -->



<?php
    elseif(get_row_layout() == "sidebar_image"):
?>
            <div class="box image">
<?php
            if(get_sub_field("sidebar_image_title")):
              echo "<h3>" . get_sub_field("sidebar_image_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_image_description")):
              the_sub_field("sidebar_image_description");
            endif;

            if(get_sub_field("sidebar_image")):
              $sidebar_image = get_sub_field("sidebar_image");
              $sidebar_image_url = $sidebar_image['url'];
              $sidebar_image_alt = $sidebar_image['alt'];
            endif;

            if(get_sub_field("sidebar_image") && get_sub_field("sidebar_image_link")):
              echo "<a href='".get_sub_field("sidebar_image_link")."'>";
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
              echo "</a>";
            endif;

            if(get_sub_field("sidebar_image") && !get_sub_field("sidebar_image_link")):
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
            endif;
// Begin Image Button
            if(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "External Link"):
?>
              <a href="<?php the_sub_field('sidebar_image_button_external_link'); ?>" target="_blank" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            elseif(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "Internal Link"):
?>
              <a href="<?php the_sub_field('sidebar_image_button_page_link'); ?>" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            endif;
// End Image Button
?>

            </div> <!-- .box -->

<?php
    elseif(get_row_layout() == "sidebar_script"):
?>
            <div class="box image">
<?php
            if(get_sub_field("sidebar_script_title")):
              echo "<h3>" . get_sub_field("sidebar_script_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_script_description")):
              the_sub_field("sidebar_script_description");
            endif;

            if(get_sub_field("sidebar_script")):
              echo the_sub_field("sidebar_script");
            endif;
?>

            </div> <!-- .box -->
<?php
    endif; // end get_row_layouts
?>
            </div> <!-- .sidebar -->
          </div> <!-- medium-6 large-4 sidebar -->

<?php
    endwhile;
  endif;
?>


  <!-- end page layout row -->
  </div> <!-- .row -->


<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
