<?php
/**
 * Template Name: Sensi
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="sensi">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>


 <!-- General Page Content -->
  <?php if(have_rows("sensi")): while(have_rows("sensi")): the_row(); ?>
    <?php if(get_row_layout() == "row:_two_cells"): ?>
      <?php $section_title = get_sub_field("section_title"); 
            $section_subtitle = get_sub_field("section_subtitle");
            $desktop_distribution = get_sub_field("desktop_content_distribution"); 
      ?>

    <section class="row cells--two">
      <?php if($section_title): ?>
        <h2><?php echo $section_title; ?></h2>
      <?php endif; ?>
      <?php if($section_subtitle): ?>
        <h3><?php echo $section_subtitle; ?></h3>
      <?php endif; ?>

      <div class="cells--container
        <?php if($desktop_distribution == 'Desktop: 1/2 -> 1/2'): ?>cell--six-six
        <?php elseif($desktop_distribution == 'Desktop: 1/3 -> 2/3'): ?>cell--four-eight
        <?php elseif($desktop_distribution == 'Desktop: 2/3 -> 1/3'): ?>cell--eight-four
        <?php endif; ?>">
      
        <?php if(have_rows("content_cells")): while(have_rows("content_cells")): the_row(); ?>
        <div class="cell">
          <?php if(have_rows("cell")): while(have_rows("cell")): the_row(); ?>
            <?php if(get_row_layout() == "image"): ?>
              <?php $image = get_sub_field("image");
                    $caption = get_sub_field("caption");
              ?>

              <?php if($image): ?>
                <figure>
                  <img src="<?php echo $image['url']; ?>">
                  <?php if($caption): ?>
                  <figcaption><?php echo $caption; ?></figcaption>
                  <?php endif; ?>
                </figure>
              <?php endif; ?>

            <?php elseif(get_row_layout() == "video"): ?>
              <?php $video = get_sub_field("video"); ?>
              <?php if($video): echo $video; endif; ?>

            <?php elseif(get_row_layout() == "editor"): ?>
              <?php $editor = get_sub_field("editor"); ?>
              <?php if($editor): echo $editor; endif; ?>

            <?php elseif(get_row_layout() == "button"): ?>
              <?php $button_link = get_sub_field("button_link");
                    $button_text = get_sub_field("button_text");
              ?>

              <?php if($button_link && $button_text): ?>
                <a href="<?php echo $button_link; ?>" class="button blue"><?php echo $button_text; ?></a>
              <?php endif; ?>

            <?php endif; ?>
          <?php endwhile; endif; ?>
        </div>
        <?php endwhile; endif; ?>
      </div> <!-- .cells--container -->
    
    </section>

    <?php elseif(get_row_layout() == "row:_three_cells"): ?>
      <?php $section_title = get_sub_field("section_title"); 
            $section_subtitle = get_sub_field("section_subtitle");
            $desktop_distribution = get_sub_field("desktop_content_distribution"); 
      ?>

    <section class="row cells--three">
      <?php if($section_title): ?>
        <h2><?php echo $section_title; ?></h2>
      <?php endif; ?>
      <?php if($section_subtitle): ?>
        <h3><?php echo $section_subtitle; ?></h3>
      <?php endif; ?>

      <div class="cells--container">
      
      <?php if(have_rows("content_cells")): while(have_rows("content_cells")): the_row(); ?>
        <div class="cell">
          <?php if(have_rows("cell")): while(have_rows("cell")): the_row(); ?>
            <?php if(get_row_layout() == "image"): ?>
              <?php $image = get_sub_field("image");
                    $caption = get_sub_field("caption");
              ?>

              <?php if($image): ?>
                <figure>
                  <img src="<?php echo $image['url']; ?>">
                  <?php if($caption): ?>
                  <figcaption><?php echo $caption; ?></figcaption>
                  <?php endif; ?>
                </figure>
              <?php endif; ?>

            <?php elseif(get_row_layout() == "video"): ?>
              <?php $video = get_sub_field("video"); ?>
              <?php if($video): echo $video; endif; ?>

            <?php elseif(get_row_layout() == "editor"): ?>
              <?php $editor = get_sub_field("editor"); ?>
              <?php if($editor): echo $editor; endif; ?>

            <?php elseif(get_row_layout() == "button"): ?>
              <?php $button_link = get_sub_field("button_link");
                    $button_text = get_sub_field("button_text");
              ?>

              <?php if($button_link && $button_text): ?>
                <a href="<?php echo $button_link; ?>" class="button blue"><?php echo $button_text; ?></a>
              <?php endif; ?>

            <?php endif; ?>
          <?php endwhile; endif; ?>
        </div>
      <?php endwhile; endif; ?>

      </div>
    
    </section>    

    <?php endif; ?>
  <?php endwhile; endif; ?>

<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
