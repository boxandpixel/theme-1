<?php
/**
 * Template Name: Watchdog Service Membership
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php //get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="watchdog-membership"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

<?php 
  if(have_rows("watchdog_membership")):  
?>

  <div class="row watchdog-top">
<?php
  while(have_rows("watchdog_membership")): the_row();
    if(get_row_layout() == "watchdog_image"):
?>
    <div class="medium-3 columns watchdog-logo">
<?php
      $image = get_sub_field("watchdog_image");
      $image_url = $image['url'];
      $image_alt = $image['alt'];

      if($image):
?>
      <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php
      endif;
?>
    </div> <!-- .medium-3 columns -->
<?php
    endif; //get_row_layout()
  endwhile; //have_rows()
?>
    <div class="medium-9 columns">
      <div class="row">

<?php 
  while(have_rows("watchdog_membership")): the_row();
    if(get_row_layout() == "watchdog_questions"):
?>
    <div class="medium-12 columns">
      <h2><?php the_sub_field("watchdog_questions_title"); ?></h2>
      <?php the_sub_field("watchdog_questions"); ?>
    </div> <!-- .medium-12 -->
<?php
    endif; // get_row_layout
  endwhile; // have_rows
?>
      </div> <!-- .row -->

      <div class="row boxes" data-equalizer data-equalize-on="medium">
<?php 
  while(have_rows("watchdog_membership")): the_row();
    if(get_row_layout() == "watchdog_boxes"):
      while(have_rows("watchdog_boxes")): the_row();
?>
    <div class="medium-4 columns">
      <div class="box" data-equalizer-watch>
        <div class="box-image">
<?php
        $image = get_sub_field("watchdog_box_image");
        $image_url = $image['url'];
        $image_alt = $image['alt'];

        if($image):
?>
          <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php
        endif;
?>
        </div> <!-- .box-image -->
        <?php the_sub_field("watchdog_box_content"); ?>
      </div> <!-- .box -->
    </div> <!-- .medium-4 -->
<?php
      endwhile; // have_rows("watchdog_boxes")
    endif; // get_row_layout
  endwhile; // have_rows
?>
      </div> <!-- .row -->
    </div> <!-- .medium-9 -->
  </div> <!-- .row -->

  <div class="row watchdog-list">
<?php 
  while(have_rows("watchdog_membership")): the_row();
    if(get_row_layout() == "watchdog_list"):
?>
    <div class="medium-12 columns">
      <?php the_sub_field("watchdog_list"); ?>
    </div> <!-- .medium-12 -->
<?php
    endif; // get_row_layout
  endwhile; // have_rows
?>
  </div> <!-- .row .watchdog-list -->

  <div class="row sign-up">
<?php
  while(have_rows("watchdog_membership")): the_row();
    if(get_row_layout() == "watchdog_sign_up"): 
?>
    <div class="medium-12 columns">
      <?php the_sub_field("sign_up_text"); ?>
      <a href="<?php the_sub_field('sign_up_link'); ?>" class="button blue"><?php the_sub_field("sign_up_link_text"); ?></a>
    </div> <!-- .medium-12 -->
<?php
    endif;
  endwhile;
?>
  </div> <!-- .row .sign-up -->

<?php       
  endif; //have_rows("watchdog_membership")
?>

<?php endwhile;?>


 </div>

 <?php get_footer(); ?>
