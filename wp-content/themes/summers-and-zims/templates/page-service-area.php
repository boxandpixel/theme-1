<?php
/**
 * Template Name: Service Area
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 

 <div id="page" role="main" class="service-area"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

  <div class="row">
    <div class="medium-12 columns">

      <div id="service-map"></div>
      <script>
          function initServiceMap() {
            var service_loc = new google.maps.LatLng(39.982797,-76.0297137)
            var service_map = new google.maps.Map(document.getElementById('service-map'), {
              center: service_loc,
              zoom: 9,
              scrollwheel: false
            });

            // Display Lancaster, Chester and Delware County
            var area = new google.maps.FusionTablesLayer({
              map: service_map,
              query: {
                select: "col4",
                from: "1xdysxZ94uUFIit9eXmnw1fYc6VcQiXhceFd_CVKa",
                where: "col2 in (\x27Lancaster\x27, \x27Chester\x27, \x27Delaware\x27) and col3 in (\x27PA-Lancaster\x27, \x27PA-Delaware\x27, \x27PA-Chester\x27)"
              },
              styles: [{
                polygonOptions: {
                  fillColor: '#0066ae',
                  fillOpacity: 0.3
                },
             }]                        
            });
            area.setMap(service_map);     

            // Define Reading Coordinates
            var readingCoords = [
              {lat: 40.31569, lng: -76.15147},
              {lat: 40.31478, lng: -76.15003},
              {lat: 40.31374, lng: -76.14839},
              {lat: 40.30316, lng: -76.13174},
              {lat: 40.29222, lng: -76.11445},
              {lat: 40.28434, lng: -76.10213},
              {lat: 40.28296, lng: -76.10001},
              {lat: 40.27609, lng: -76.08916},
              {lat: 40.26641, lng: -76.07386},
              {lat: 40.26389, lng: -76.06986},
              {lat: 40.25994, lng: -76.06389},
              {lat: 40.24817, lng: -76.04552},
              {lat: 40.24552, lng: -76.04138},
              {lat: 40.24129, lng: -76.03476},
              {lat: 40.22931, lng: -76.01586},
              {lat: 40.21738, lng: -75.99718},
              {lat: 40.21604, lng: -75.99507},
              {lat: 40.2134, lng: -75.99095},
              {lat: 40.20874, lng: -75.98378},
              {lat: 40.20456, lng: -75.97728},
              {lat: 40.1836, lng: -75.94482},
              {lat: 40.1758, lng: -75.93299},
              {lat: 40.17449, lng: -75.93099},
              {lat: 40.17233, lng: -75.92768},
              {lat: 40.15686, lng: -75.90392},
              {lat: 40.1499, lng: -75.89326},
              {lat: 40.13792, lng: -75.87478},
              {lat: 40.13738, lng: -75.87377},           
              {lat: 40.13738, lng: -75.87377},
              {lat: 40.138, lng: -75.87245}, 
              {lat: 40.14217, lng: -75.86556},
              {lat: 40.14581, lng: -75.85993},
              {lat: 40.14748, lng: -75.85737},
              {lat: 40.14884, lng: -75.85526},
              {lat: 40.15121, lng: -75.85113},
              {lat: 40.15473, lng: -75.84501},
              {lat: 40.16078, lng: -75.83525},
              {lat: 40.16842, lng: -75.82305},
              {lat: 40.18122, lng: -75.80197},
              {lat: 40.18779, lng: -75.79115},
              {lat: 40.18939, lng: -75.78853},
              {lat: 40.19176, lng: -75.78455},
              {lat: 40.19356, lng: -75.78155},
              {lat: 40.20201, lng: -75.76755},
              {lat: 40.20588, lng: -75.76115},
              {lat: 40.21026, lng: -75.75389},
              {lat: 40.21166, lng: -75.75156},
              {lat: 40.2193, lng: -75.73874},
              {lat: 40.22502, lng: -75.72885},
              {lat: 40.22682, lng: -75.72544},
              {lat: 40.23027, lng: -75.7189},
              {lat: 40.23448, lng: -75.71093},
              {lat: 40.23919, lng: -75.70192},
              {lat: 40.24099, lng: -75.69844},
              {lat: 40.24186, lng: -75.69682},
              {lat: 40.24186, lng: -75.69678},
              {lat: 40.371738, lng: -75.926665}
            ];            

            // Build the Reading, PA Polygon
            var readingArea = new google.maps.Polygon({
              map: service_map,
              paths: readingCoords,
              strokeColor: '#666666',
              strokeOpacity: 0.4,
              strokeWeight: 2,
              fillColor: 'hsl(205, 60%, 34%)',
              fillOpacity: 0.5
            });
            readingArea.setMap(map);




          }                                                                          
      </script> 

    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
