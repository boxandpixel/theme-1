<?php
/**
 * Template Name: Contact Us
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="contact-us"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

  <div class="row">
    <div class="medium-6 medium-push-6 columns">
      <div class="form-warning">
        <?php the_field('contact_warning'); ?>
      </div> <!-- .form-warning -->
      <div class="contact-video">
        <?php the_field("contact_video"); ?>
      </div>

      <br> <!-- Very temporary -->
      <?php the_field('contact_testimonial'); ?>

           

    </div> <!-- .medium-6 -->
    <div class="medium-6 medium-pull-6 columns">
      <?php the_field('contact_form'); ?>
    </div> <!-- .medium-6 -->
  </div> <!-- .row -->

<?php endwhile;?>


 </div>

 <?php get_footer(); ?>
