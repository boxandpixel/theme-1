<?php
/**
 * The template for displaying pages
 *
 * Template Name: Product Checkout 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="product-checkout">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

	<div class="row">
		<div class="small-12<?php if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :?> medium-6<?php endif;?> columns">
			<?php dimox_breadcrumbs(); ?>
		</div> <!-- .small-12 -->
		<?php 
			if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :	
		?>
		<div class="small-12 medium-6 columns cart-active">
			<?php echo '<a href="' . $woocommerce->cart->get_cart_url() . '" class="view-cart" title="View Cart">(' . $woocommerce->cart->get_cart_contents_count() . ')</a>';?>
		</div>
		<?php endif; ?>	
	</div> <!-- .row -->	

  <div class="row">
    <div class="small-12 columns">
    	<h1><?php the_title(); ?></h1>
        
        <div class="woocontent">    
    	<?php the_content(); ?>
	    </div> <!-- .woocontent -->

    </div> <!-- .small-12 -->
  </div> <!-- .row -->    
 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>
 <?php get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
