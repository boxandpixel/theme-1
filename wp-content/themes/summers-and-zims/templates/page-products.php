<?php
/**
 * Template Name: Products
 *
 */

get_header(); ?>

<div id="page" role="main" class="product-pages"> 

	<div class="row">
		<div class="small-12<?php if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :?> medium-6<?php endif;?> columns">
			<?php dimox_breadcrumbs(); ?>
		</div> <!-- .small-12 -->
		<?php 
			if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :	
		?>
		<div class="small-12 medium-6 columns cart-active">
			<?php echo '<a href="' . $woocommerce->cart->get_cart_url() . '" class="view-cart" title="View Cart">(' . $woocommerce->cart->get_cart_contents_count() . ')</a>';?>
		</div>
		<?php endif; ?>				
	</div> <!-- .row -->	
	Yo
	<div class="row">
		<div class="small-12 columns">

		<?php //do_action( 'foundationpress_before_content' ); ?>

		<?php while ( woocommerce_content() ) : the_post(); ?>

		

		<?php endwhile;?>

		<?php //do_action( 'foundationpress_after_content' ); ?>

		</div> <!-- .small-12 -->
	</div> <!-- .row -->

</div> <!-- #page -->


<?php get_footer(); ?>
