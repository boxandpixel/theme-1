<?php
/**
 * Template Name: Service
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="service"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 

 <!-- Service Page -->

  <div class="row">

		<div class="medium-6 large-8 columns service-main">

      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?> 

<?php
  if(have_rows('service_page')):        
    while(have_rows('service_page')): the_row();
      if(get_row_layout() == 'service_main_layout'):

?>
<?php
        the_sub_field('service_text');

        the_sub_field('service_video');

        if(have_rows('service_images')):
          
?>

      <div class="row">
<?php
          while(have_rows('service_images')): the_row();
            if(get_sub_field('service_image') != ""):
              $service_image = get_sub_field('service_image');
              $service_image_url = $service_image['url'];
              $service_image_alt = $service_image['alt'];
              $service_image_small = $service_image['sizes']['small'];
              $service_image_medium = $service_image['sizes']['medium'];
              $service_image_large = $service_image['sizes']['large'];
              $service_image_xlarge = $service_image['sizes']['x-large'];                 
?>
        <div class="large-6 columns service-images">
          <img
            src="<?php echo $service_image_small; ?>"
            srcset="<?php echo $service_image_medium; ?> 360w, 
                    <?php echo $service_image_large; ?> 570w,
                    <?php echo $service_image_xlarge; ?> 740w"
            sizes="(min-width: 770px) 370px, 740px"

            alt="<?php echo $service_image_alt; ?>"
          >           
        </div> <!-- .large-6 -->
<?php
            endif; // if(get_sub_field('service_image')
          endwhile; // while(have_rows('service_images'))
?>
      </div> <!-- .row -->
<?php 
        endif; // if(have_rows('service_images')

      elseif(get_row_layout() == "service_general_content"):
        the_sub_field("general_content");

      elseif(get_row_layout() == "service_link_blocks"):

        if(have_rows("link_blocks")):
?>
          <ul class="link-blocks">
<?php                    
          while(have_rows("link_blocks")): the_row();

            if(get_sub_field("link_or_text") == "Links to Page"):
?>
            <li>
              <div class="flipper">
                <div class="front"><a href="<?php the_sub_field('link_location'); ?>"><span><?php the_sub_field("link_text"); ?></span></a></div>
                <div class="back"><a href="<?php the_sub_field('link_location'); ?>"><span>Learn More</span></a></div>
              </div>
            </li>
<?php
            elseif(get_sub_field("link_or_text") == "Text Block Only"):
?>
            <li class="block-text"><?php the_sub_field("link_text"); ?></li>
<?php 
            endif;

          endwhile;
?>
          </ul>
<?php          
        endif;

      elseif(get_row_layout() == "service_accordion"): 
?>
        <div class="service-accordion">
<?php
        if(have_rows("accordion")):
?>
          <ul class="acc">
<?php                    
          while(have_rows("accordion")): the_row();
?>
            <li class="panel"> 
                <h4><?php the_sub_field("heading"); ?></h4>
                <div class="panel-content">
                  <?php the_sub_field("content"); ?>
                </div>
            </li>
<?php            

          endwhile;
?>        
          </ul>
<?php      
        endif;
?>
        </div>
<?php        
      endif; // if(get_row_layout() == 'service_main_layout'):
    endwhile; // while(have_rows('service_page'))
  endif; // if(have_rows('service_page'))
?>
    </div> <!-- .medium-6 large-8 -->

    <div class="medium-6 large-4 columns">
      <div class="service-sidebar">
<?php
  if(have_rows('sidebar_content')):
    while(have_rows('sidebar_content')): the_row();
?>

<?php
    if(get_row_layout() == "sidebar_review"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_review_title")):
              echo "<h3>" . get_sub_field("sidebar_review_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_review")):
              the_sub_field("sidebar_review");
            endif;

            if(get_sub_field("sidebar_review_link")):
              echo "<a href='".get_sub_field("sidebar_review_link")."' class='button blue'>" . get_sub_field("sidebar_review_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->  

<?php
    elseif(get_row_layout() == "sidebar_membership"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_membership_title")):
              echo "<h3>" . get_sub_field("sidebar_membership_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_membership_description")):
              the_sub_field("sidebar_membership_description");
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box --> 

<?php
    elseif(get_row_layout() == "sidebar_posts"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_posts_title")):
              echo "<h3>" . get_sub_field("sidebar_posts_title") . "</h3>";
            endif;

            if(have_rows("sidebar_posts")):
              while(have_rows("sidebar_posts")): the_row();

                $posts = get_sub_field('sidebar_post');

                if($posts):
?>
          <article class="service-posts">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <footer>
              Posted on <?php the_date(); ?> in <?php the_category(); ?>
            </footer>
            <?php endforeach; ?>
          </article>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php 
                endif;                
              endwhile;
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>
        </div> <!-- .box -->         

<?php
    elseif(get_row_layout() == "sidebar_appointment"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_appointment_title")):
              echo "<h3>" . get_sub_field("sidebar_appointment_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_appointment_description")):
              the_sub_field("sidebar_appointment_description");
            endif;

            if(get_sub_field("sidebar_appointment_link")):
              echo "<a href='".get_sub_field("sidebar_appointment_link")."' class='button blue'>" . get_sub_field("sidebar_appointment_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->                     



<?php
    elseif(get_row_layout() == "sidebar_image"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_image_title")):
              echo "<h3>" . get_sub_field("sidebar_image_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_image_description")):
              the_sub_field("sidebar_image_description");
            endif;

            if(get_sub_field("sidebar_image")):
              $sidebar_image = get_sub_field("sidebar_image");
              $sidebar_image_url = $sidebar_image['url'];
              $sidebar_image_alt = $sidebar_image['alt'];
            endif;

            if(get_sub_field("sidebar_image") && get_sub_field("sidebar_image_link")):
              echo "<a href='".get_sub_field("sidebar_image_link")."'>";
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
              echo "</a>";
            endif;

            if(get_sub_field("sidebar_image") && !get_sub_field("sidebar_image_link")):
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
            endif;
// Begin Image Button
            if(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "External Link"):
?>
              <a href="<?php the_sub_field('sidebar_image_button_external_link'); ?>" target="_blank" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            elseif(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "Internal Link"):
?>              
              <a href="<?php the_sub_field('sidebar_image_button_page_link'); ?>" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            endif;
// End Image Button            
?>

        </div> <!-- .box -->   

<?php
    elseif(get_row_layout() == "sidebar_script"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_script_title")):
              echo "<h3>" . get_sub_field("sidebar_script_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_script_description")):
              the_sub_field("sidebar_script_description");
            endif;

            if(get_sub_field("sidebar_script")):
              echo the_sub_field("sidebar_script"); 
            endif;            
?>

        </div> <!-- .box -->        
<?php
    endif; // end get_row_layouts
?>
       
<?php
    endwhile;
  endif;
?>
      </div> <!-- .sidebar -->
    </div> <!-- medium-6 large-4 sidebar -->
  </div> <!-- .row -->    



<?php endwhile;?>



 <?php //do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
