<?php
/**
 * Template Name: Service Memberships
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="service-memberships"> 	

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?> 


  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php dimox_breadcrumbs(); ?>       
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

  <div class="row">
    <div class="small-12 columns">
      <div class="service-membership-intro">
        <div class="intro-copy">
          <?php the_content(); ?>
        </div>
        <div class="intro-video">
          <?php the_field("service_membership_video"); ?>
        </div>
      </div>
    </div>
  </div>


<?php 

  if(have_rows("service_membership_page")):
    while(have_rows("service_membership_page")): the_row();

      if(get_row_layout() == "membership_table"):
?>
      <div class="row membership-table">
        <div class="medium-12 columns">
          <h2><?php the_sub_field("membership_table_title"); ?></h2>        
          <table border="0" class="stack">
            <col>
            <colgroup span="1"></colgroup>
            <colgroup span="2"></colgroup>

<?php
        if(have_rows("table_headers")):
?>
            <thead>
              <tr>
                <td rowspan="2"></td>
<!--                 <th colspan="1" scope="colgroup">Non-Member Customers</th> -->
                <th colspan="2" scope="colgroup">Membership Customers</th>
              </tr>
              <tr>
<?php
          while(have_rows("table_headers")): the_row();
?>  
                <th scope="col">
<?php
            $table_image = get_sub_field("header");
            $table_image_url = $table_image['url'];
            $table_image_alt = $table_image['alt'];

            $link = get_sub_field("header_link");

            if($link):
?>
                  <a href="<?php echo $link; ?>">
                    <img src="<?php echo $table_image_url; ?>" alt="<?php echo $table_image_alt; ?>">
                  </a>
<?php
            else:
?>
                  <img src="<?php echo $table_image_url; ?>" alt="<?php echo $table_image_alt; ?>">
<?php
            endif;
?>
                </th>       
<?php
          endwhile;
?>
              </tr>
            </thead>
<?php
        endif;
?>

<?php
        if(have_rows("table_rows")):
?>
            <tbody>
              
<?php
          while(have_rows("table_rows")): the_row();
?> 
              <tr> 
                <td>
                  <?php the_sub_field("service_description") ?>
                </td>   
<!--                 <td>
                  <?php //the_sub_field("tune-up_special") ?>
                </td>  -->   
                <td>
                  <?php the_sub_field("blue_plan_watchdog") ?>
                </td>     
                <td>
                  <?php the_sub_field("gold_plan_watchdog") ?>
                </td>
              </tr>                                                                                 
<?php
          endwhile;
?>
            </tbody>
<?php
        endif;
?>


          </table>
          <div class="table-caption">
            <?php the_sub_field("membership_table_caption"); ?>
          </div> <!-- .table-caption -->          
        </div> <!-- .medium-12 -->
      </div> <!-- .row -->   
<?php
    elseif(get_row_layout() == "membership_benefits"):
?>
    <div class="row benefits-documents">
      <div class="medium-6 large-8 columns benefits">
          <h3><?php the_sub_field("membership_benefits_title"); ?></h3>
<?php
          if(have_rows("membership_benefits")):
            while(have_rows("membership_benefits")): the_row();
?>
            <?php the_sub_field("membership_benefit"); ?>
<?php
            endwhile;
          endif;
?>
      </div> <!-- .medium-6 -->
<?php 
    elseif(get_row_layout() == "membership_documents"):
?>
      <div class="medium-6 large-4 columns documents">
        <h3><?php the_sub_field("membership_documents_title"); ?></h3>
        <?php the_sub_field("membership_documents"); ?>
      </div> <!-- .medium-6 -->
    </div> <!-- .row -->

<?php
    elseif(get_row_layout() == "membership_sign_up"):
?>
    <div class="row sign-up">
      <div class="medium-12 columns">
        <?php the_sub_field("sign_up_text"); ?>
        <a href="<?php the_sub_field('sign_up_link'); ?>" class="button blue"><?php the_sub_field("sign_up_link_text"); ?></a>
      </div> <!-- .medium-12 -->
    </div> <!-- .row -->

<?php
    endif; // get_row_layout()
    endwhile;
  endif;
?>


<?php endwhile;?>


 </div>

 <?php get_footer(); ?>
