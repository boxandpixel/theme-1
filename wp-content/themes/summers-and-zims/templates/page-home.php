<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="home">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

 <!-- start home page -->
<?php
  if(have_rows('home_page')):
    while(have_rows('home_page')): the_row();

      if(get_row_layout() == 'box_row'):
?>
      <div class="row boxes" data-equalizer data-equalize-on="large">
<?php
        if(have_rows('box')):
          while(have_rows('box')): the_row();
?>
        <div class="medium-4 columns">
          <div class="home-box" data-equalizer-watch>

            <h3><?php the_sub_field('box_title');?></h3>
<?php
            if(get_sub_field('box_image')):
?>
            <div class="home-box-image">
<?php
          // Box Image: Get and display from array
          $box_image = get_sub_field('box_image');
          $box_image_alt = $box_image['alt'];
          $box_image_small = $box_image['sizes']['small'];
          $box_image_medium = $box_image['sizes']['medium'];
          $box_image_large = $box_image['sizes']['large'];
          $box_image_xlarge = $box_image['sizes']['x-large'];

?>



<?php
            if(get_sub_field('box_link')):
?>
              <a href="<?php the_sub_field('box_link'); ?>">
<?php
            elseif(get_sub_field("box_link_external")):
?>
              <a href="<?php the_sub_field('box_link_external'); ?>" target="_blank">
<?php
            endif;
?>

              <img
                src="<?php echo $box_image_medium; ?>"
                srcset="<?php echo $box_image_medium; ?> 360w, 
                        <?php echo $box_image_large; ?> 570w,
                        <?php echo $box_image_xlarge; ?> 740w"
                sizes="(min-width: 770px) 370px, 740px"

                alt="<?php echo $box_image_alt; ?>"
              >
              </a>
            </div> <!-- .home-box-image -->
<?php
            endif;

            if(get_sub_field('box_review')):
?>
              <div class="home-box-review">
                <div class="home-box-trans">
<?php
              the_sub_field('box_review');
?>
                </div> <!-- .home-box-trans -->
              </div> <!-- .home-box-review -->
<?php
            endif;
?>
<?php
            if(get_sub_field('box_video')):
?>
              <div class="home-box-video">
<?php
              the_sub_field('box_video');
?>
              </div> <!-- .home-box-review -->
<?php
            endif;
?>
            <div class="home-box-bottom">

              <?php the_sub_field('box_description'); ?>
<?php
            if(get_sub_field('box_link')):
?>
              <a href="<?php the_sub_field('box_link'); ?>" class="button blue"><?php the_sub_field('box_link_text'); ?></a>

<?php

            elseif(get_sub_field('box_link_external')):
?>
              <a href="<?php the_sub_field('box_link_external'); ?>" class="button blue" target="_blank"><?php the_sub_field('box_link_text'); ?></a>
<?php
            endif;
?>
            </div> <!-- .home-box-bottom -->
          </div> <!-- .home-box -->
        </div> <!-- .medium-4 columns -->
<?php
          endwhile;
        endif;
?>
      </div> <!-- .row-->
<?php
      //endif; // get_row_layout() == "box_row";
?>
<?php
      elseif(get_row_layout() == 'staff'):
?>
  <div class="row staff collapse">
    <div class="small-12 columns">
<?php
      $staff_truck = get_sub_field("staff_truck");
?>
      <img src="<?php echo $staff_truck['url']; ?>" alt="" id="staff-truck">

      <h3><?php the_sub_field("staff_title"); ?></h3>

      
<?php
      if(have_rows("staff_members")):
?>
      <div class="staff-members">   
<?php          
        while(have_rows("staff_members")): the_row();
           
          $staff_member = get_sub_field('staff_member');
          if($staff_member):
            $post = $staff_member;
            setup_postdata($post);

            // Get images
            $staff_image = get_field("thumbnail_image");
            $staff_image_thumb = $staff_image['sizes']['thumbnail'];
            $staff_image_staff_thumb = $staff_image['sizes']['staff-thumb'];
            $staff_image_small = $staff_image['sizes']['small'];

?>

          <div class="staff-member">
            <img
                src="<?php echo $staff_image_thumb; ?>"
                srcset="
                  <?php echo $staff_image_thumb; ?> 120w,
                  <?php echo $staff_image_staff_thumb; ?> 180w,
                  <?php echo $staff_image_small; ?> 220w
                "
            >
            <a href="<?php the_permalink(); ?>">
              <?php the_title(); ?>
              <span class="job-title"><?php the_field("job_title"); ?></span>    
            </a>
          </div> <!-- .staff-member -->
      
<?php
            wp_reset_postdata();
          endif;

        endwhile;
?>
      </div> <!-- .staff-members -->


<?php        
      endif;
      
      $staff_button = get_sub_field("staff_button");
      if($staff_button):
        
?>

    <a href="<?php echo $staff_button['staff_button_link'] ?>" class="button green"><?php echo $staff_button['staff_button_text']; ?></a>     

<?php
      endif;
?>        
    </div> <!-- .small-12 columns -->
  </div> <!-- .row .staff -->

<?php
      elseif(get_row_layout() == "slides"):

?>
      <div class="row slides-difference collapse">
<?php


?>
        <div class="small-12 columns slides">
          <h3><?php the_sub_field('slides_section_title'); ?></h3>
          <?php //the_sub_field('slides_section_description'); ?>
<?php
          if(have_rows('slide')):


?>
          <ul class="slides-case">
<?php
            while(have_rows('slide')): the_row();
            $slide_image = get_sub_field("slide_image");
            $slide_image_alt = $slide_image['alt'];
            $slide_image_medium = $slide_image['sizes']['medium'];
            $slide_image_large = $slide_image['sizes']['large'];
            $slide_image_xlarge = $slide_image['sizes']['x-large'];
?>

            <li>
              <div class="slide-media">

                  <?php
                    $slide_type = get_sub_field("slide_type");
                    if($slide_type == "Image"):
                    ?>
                      <img
                        src="<?php echo $slide_image['url']; ?>"
                        srcset="
                          <?php echo $slide_image_medium; ?> 360w,
                          <?php echo $slide_image_large; ?> 570w,
                          <?php echo $slide_image_xlarge; ?> 740w"

                        sizes="(min-width: 600px) 740px, (min-width: 768px) 570px, 360px"
                      

                        alt="<?php echo $slide_image_alt; ?>"
                      >
                    <?php
                    elseif($slide_type == "Video oEmbed"):
                      the_sub_field("slide_video");
                    endif;

                  ?>
                </div>
                <div class="slide-content">
                  <div class="content">
                    <h4><?php the_sub_field("slide_title"); ?></h4>
                    <?php the_sub_field('slide_content') ?>
                  </div>
                </div>
            </li>
<?php
            endwhile;
?>
          </ul>
<?php
          endif;

?>
</div> <!-- .small-12 -->
</div> <!-- .row -->
<?php
      //endif;

      elseif(get_row_layout() == "two_sections"):
?>
      <div class="row two-sections">
<?php
      if(have_rows("section_group")):
        while(have_rows("section_group")): the_row();
?>
        <div class="medium-6 columns">
        
<?php
        $section_options = get_sub_field("section_options");
        if($section_options = "Has Title"):
?>
          <h3><?php the_sub_field("section_title"); ?></h3>
<?php
        endif;
        if($section_options = "Has Intro"):
          the_sub_field("section_intro");
        endif;
?>
<?php
        $section_type = get_sub_field("section_type");
        if($section_type == "Slides"):

          if(have_rows("slides")):
?>
            <ul class="section-slides">
<?php
            while(have_rows("slides")): the_row();
              $slide_background = get_sub_field("slide_background_image");
?>
              <li>
<?php

?>

                <div class="slide-content">
                  <h4><?php the_sub_field("slide_title"); ?></h4>
                  <?php the_sub_field("slide_content"); ?>
                </div> <!-- .slide-content -->
              </li>
<?php
            endwhile;
?>
            </ul>
<?php
          endif;
        elseif($section_type == "Posts"):
          if(have_rows('posts')):
            while(have_rows('posts')): the_row();


              $blog_post = get_sub_field('post');
              if($blog_post):
                $post = $blog_post;
                setup_postdata($post);
?>
          <article class="post">
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <footer>
              Posted on <?php the_date(); ?> <!--in--> <?php //the_category(); ?>
            </footer>
          </article>

<?php


                wp_reset_postdata();
              endif;
            endwhile;
          endif;
        elseif($section_type == "Editor"):
          the_sub_field("editor");
        endif;
?>

        </div>
<?php
        endwhile;
      endif;
?>
      </div>

<?php
      //endif;

      elseif(get_row_layout() == "brands"):
?>
      <div class="row">
        <div class="medium-12 columns brands">
          <h3><?php the_sub_field('brands_section_title'); ?></h3>
        <?php the_sub_field('brands_section_description'); ?>
<?php
        if(have_rows('brands')):
?>
          <ul class="brands-list">
<?php
          while ( have_rows('brands') ) : the_row();
          $brand_image = get_sub_field('brand');
          $brand_image_alt = $brand_image['alt'];
          $brand_image_180 = $brand_image['sizes']['thumbnail'];
          $brand_image_320 = $brand_image['sizes']['small'];
          $brand_image_480 = $brand_image['sizes']['medium'];
?>
            <li>
              <img
                src="<?php echo $brand_image_180; ?>"
                srcset="<?php echo $brand_image_180; ?> 180w"
                sizes="(max-width: 640px) 180px, (min-width: 48em) 180px, 180px"

                alt="<?php echo $brand_image_alt; ?>"
              >
            <li>
<?php
          endwhile;
?>
          </ul>
<?php
        endif;
?>
        </div> <!-- .medium-12 -->
      </div> <!-- .row -->
<?php
      elseif(get_row_layout() == "general_content_full"):

        $full_width = get_sub_field("full_width_content");
        $full_width_image = get_sub_field("full_width_background");
        $full_width_medium = $full_width_image['sizes']['medium'];
        $full_width_large = $full_width_image['sizes']['large'];
        $full_width_xlarge = $full_width_image['sizes']['x-large'];



?>
      <div class="row gc-content collapse">
        <div class="small-12 columns">
          <div class="gc-content-full" style="background: url(<?php echo $full_width_large; ?>) center no-repeat; background-size: cover;">
            <div class="trans">
              <div class="content">
                <?php echo $full_width; ?>
              </div>
            </div>
          </div>
        </div> <!-- .small-12 columns -->
      </div> <!-- .row.gc-full -->
<?php

      elseif(get_row_layout() == "general_content_split_width"):

        $left_background = get_sub_field('left_background');
        $left_background_large = $left_background['sizes']['large'];

        $left_button_text = get_sub_field("left_button_text");
        $left_button_link = get_sub_field("left_button_link")['url'];

        $right_background = get_sub_field('right_background');
        $right_background_large = $right_background['sizes']['large'];

        $right_button_text = get_sub_field("right_button_text");
        $right_button_link = get_sub_field("right_button_link")['url'];


        
?>
      <div class="row gc-content" data-equalizer data-equalize-on="large">

        <div class="small-12 medium-6 columns">
          <div class="gc-content-left" style="background: url(<?php echo $left_background_large; ?>) center no-repeat; background-size: cover;" data-equalizer-watch>
            <div class="trans">
              
              <h2><?php the_sub_field("left_section_title"); ?></h2>
              <div class="content">
                <?php the_sub_field("left_content"); ?>

                <?php
                          if($left_button_text || $left_button_link):
                ?> 
                          <a href="<?php echo esc_url($left_button_link); ?>" class="button white"><?php the_sub_field("left_button_text"); ?></a>
                <?php            
                          endif;
                ?>
              </div>


            </div>          
          </div>       
        </div> <!-- .small-12 -->
        <div class="small-12 medium-6 columns">
          <div class="gc-content-right" style="background: url(<?php echo $right_background_large; ?>) center no-repeat; background-size: cover;" data-equalizer-watch>
          
            <div class="trans">
              <h2><?php the_sub_field("right_section_title"); ?></h2>
              <div class="content">
                <?php the_sub_field("right_content"); ?>

                <?php
                          if($right_button_text || $right_button_link):
                ?> 
                          <a href="<?php echo esc_url($right_button_link); ?>" class="button white"><?php the_sub_field("right_button_text"); ?></a>
                <?php            
                          endif;
                ?>
              </div>
            </div>
          </div>           
        </div> <!-- .small-12 -->

      </div> <!-- .row.gc-split -->
<?php
      endif;

    endwhile;
  endif;
?>
 <!-- end home page -->




 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>
 <?php //get_sidebar(); ?>




 </div>

 <?php get_footer(); ?>
