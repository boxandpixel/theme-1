<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<footer id="footer" class="site-footer" role="contentinfo">
				<div class="row">
					<div class="medium-6 columns">
                        <div class="section">
                            <p>&copy; Copyright <?php echo date("Y"); ?>, Summers &amp; Zim's. All Rights Reserved.</p>
                        </div> <!-- .footer-section -->                        
<?php
                        if(have_rows("footer_left", "options")):
 
                            while(have_rows("footer_left", "options")): the_row();
              
                                if(get_row_layout() == "visual_editor_section"):
?>
                        <div class="section">
                            <?php the_sub_field("custom_visual_editor", "options"); ?>
                        </div> <!-- .footer-section -->
<?php
                                elseif(get_row_layout() == "plain_text_section"):
?>                                
                        <div class="footer-section">
                            <p><?php the_sub_field("custom_plain_text", "options"); ?></p>
                        </div>
<?php                         
                                endif;

                            endwhile;
                        endif;
?>                        					
					</div> <!-- .medium-6 columns -->
					<div class="medium-6 columns">
<?php
                        if(have_rows("footer_right", "options")):
 
                            while(have_rows("footer_right", "options")): the_row();
              
                                if(get_row_layout() == "visual_editor_section"):
?>
                        <div class="section">
                            <?php the_sub_field("custom_visual_editor", "options"); ?>
                        </div> <!-- .footer-section -->
<?php
                                elseif(get_row_layout() == "plain_text_section"):
?>                                
                        <div class="section">
                            <?php the_sub_field("custom_plain_text", "options"); ?>
                        </div>
<?php
                                elseif(get_row_layout() == "map_section"):
?>                                
                        <div class="section">
                            <div id="footer-map">
                                <?php //the_sub_field("map_field", "options"); ?>
                                <div id="map"></div>
                                <script>
                                    
                                    function initMap() {
                                        var sumzim = {lat: 39.949127, lng: -75.974558};
                                        var map = new google.maps.Map(document.getElementById('map'), {
                                            center: {lat: 39.949700, lng: -75.974558},
                                            zoom: 17,
                                            scrollwheel: false
                                        });
                                        var marker = new google.maps.Marker({
                                            position: sumzim,
                                            map: map
                                        }); 

                                        var loc = "<h3 style='font-size: 14px; color: black; font-weight: bold; margin: 0;'><?php the_sub_field('map_location', 'options'); ?></h3>";
                                        var street = "<p style='color: #333; margin: 0;'><?php the_sub_field('map_address_1', 'options') ?></p>";
                                        var csz = "<p style='color: #333; margin: 0;'><?php the_sub_field('map_address_2', 'options') ?></p>";
                                        var link = "<a href='<?php the_sub_field('map_link_url', 'options') ?>' target='_blank'><?php the_sub_field('map_link_text', 'options'); ?></a>";
                                        var infowindow = new google.maps.InfoWindow({

                                            content: loc + street + csz + link
                                        });

                                        infowindow.open(map, marker);                                                                           
                                    }
                                </script>                                
                            </div> <!-- #footer-map -->
                        </div> 
<?php
                                elseif(get_row_layout() == "social_icon_section"):
?>                                
                        <div class="section">
                            
<?php 
                            if(have_rows("social_icons", "options")):
?>
                            <ul class="social">
<?php                                                            
                                while(have_rows("social_icons", "options")): the_row();

                                $social_image = get_sub_field("social_icon");
                                $social_image_url = $social_image['url'];
                                $social_image_alt = $social_image['alt'];

                                
?>
                                <li>
                                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank"><img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_url; ?>"></a>
                                </li>
<?php                                 
                                endwhile;
                            endif;
?>
                            
                        </div>                                                
<?php                         
                                endif;

                            endwhile;
                        endif;
?>
					</div> <!-- .medium-6 columns -->
				</div> <!-- .row -->
			</footer><!-- #colophon -->
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<script>
    function loadMaps() {
        if (typeof initMap == 'function') { 
          initMap();
        }
        if (typeof initServiceMap == 'function') { 
          initServiceMap();
        }   
    }
</script>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-9582937-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>

<?php 
     //onclick="ga('send', 'event', 'Contact Form', 'Send Message', 'Contact Us');"     
    if(is_page("Contact Us")) {
?>        
<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    ga('send', 'event', 'Contact Form', 'Send Message', 'Contact Us');
}, false );
</script>

<?php        
    } // end if(is_page)
?>

<!-- Google Fonts -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
  WebFont.load({
    google: {
      families: ['Open Sans:300,400,600,700,800']
    }
  });
</script>


<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEgEBKltqyDUZPYxhSgHsNARDUXBQ0f2I&callback=loadMaps"
    async defer></script>

<!-- SEO tracking script -->

<script src="//cdn.leadmanagerfx.com/js/mcfx/1141" type="text/javascript"></script>
<script type='text/javascript' src='//cdn.leadmanagerfx.com/phone/js/1141'></script>

<!-- /SEO tracking script -->
    
</body>



<!-- phone insertion script begins -->

<script>

function ybFun_CustomFindAndReplace(searchText, replacement, searchNode) {
    var qsParm = ybFun_RetreiveQueryParams();
    if (!searchText || typeof replacement === 'undefined') {
        return;
    }

    var targetNum = searchText.toString();
    var provisionNum = replacement.toString();
    var delims = new Array();
    delims[0] = "";
    delims[1] = "-";
    delims[2] = ".";
    for (var i = 0; i < delims.length; i++) {
        var delimToUse = delims[i];
        var newTargetNum = targetNum.substring(1, 4) + delimToUse + targetNum.substring(4, 7) + delimToUse + targetNum.substring(7, 11);
        var newProvisionNum = provisionNum.substring(1, 4) + delimToUse + provisionNum.substring(4, 7) + delimToUse + provisionNum.substring(7, 11);
        ybFun_GenericFindAndReplace(newTargetNum, newProvisionNum);
    }
    var newTargetNum = "\\(" + targetNum.substring(1, 4) + "\\)\\s*"+ targetNum.substring(4, 7) + "-" + targetNum.substring(7, 11);
    var newProvisionNum = "(" + provisionNum.substring(1, 4)  + ") "+  provisionNum.substring(4, 7) + "-" +  provisionNum.substring(7, 11);
    ybFun_GenericFindAndReplace(newTargetNum, newProvisionNum);
}

function ybFun_GenericFindAndReplace(searchText, replacement, searchNode) {
    var regex = typeof searchText === 'string' ? new RegExp(searchText, 'g') : searchText;
    var bodyObj = document.body;
    var content = bodyObj.innerHTML;
    if (regex.test(content)) {
        content = content.replace(regex, replacement);
        bodyObj.innerHTML = content;
    }
}

function ybFun_RetreiveQueryParams() {
    var qsParm = new Array();
    var query =decodeURIComponent(parent.document.location.href);
    query = query.substring(query.indexOf('?') + 1, query.length);
    var parms = query.split('&');
    for (var i = 0; i < parms.length; i++) {
        var pos = parms[i].indexOf('=');
        if (pos > 0) {
            var key = parms[i].substring(0, pos);
            var val = parms[i].substring(pos + 1);
            val = val.replace("#", "");
            qsParm[key] = val;
        }
    }
    return qsParm;
}

var ybFindPhNums = [];
var ybReplacePhNums = [];

var ybFindCustomText = [];
var ybReplaceCustomText = [];

function ybFun_ReplaceText() {
    var qsParm = ybFun_RetreiveQueryParams();
    var useYB = qsParm['useYB'];

    var cookieUseYB = null;
    if (useYB == null) {
        cookieUseYB = ybFun_ReadCookie("useYB");
        if (cookieUseYB != null) {
            useYB = cookieUseYB;
        }
    }

    if (useYB != null) {
        ybFun_CreateCookie("useYB", useYB);
        if (ybFindPhNums == null || ybReplacePhNums == null || ybFindPhNums.length == 0 || ybReplacePhNums.length == 0
                || ybFindPhNums.length != ybReplacePhNums.length) {
            return;
        }
        if (ybFindCustomText != null && ybReplaceCustomText != null) {
            if (ybFindCustomText.length != ybReplaceCustomText.length) {
                return;
            }
        }

        if (useYB == '') {
            for (var i = 0; i < ybFindPhNums.length; i++) {
                ybFun_CustomFindAndReplace(ybFindPhNums[i], ybReplacePhNums[i]);
            }

        } else {
            var idxs = useYB.split(',');
            for (var i = 0; i < idxs.length; i++) {
                if (ybFun_IsDigit(idxs[i])) {
                    ybFun_CustomFindAndReplace(ybFindPhNums[(idxs[i] - 1)], ybReplacePhNums[(idxs[i] - 1)]);
                }
            }
        }
    }
}

function ybFun_IsDigit(strVal) {
    var reg = new RegExp("^[0-9]$");
    return (reg.test(strVal));
}

function ybFun_CreateCookie(name, value, days) {
    if (days == null) {
        days = 90;
    }
    var date = new Date();
    date.setTime( date.getTime() + (days * (24*60*60*1000)) );
    var expires = "; expires="+date.toGMTString();
    document.cookie = name + "=" + value + expires + "; path=/";
}

function ybFun_ReadCookie(name) {
    var nameLookup = name;
    var cookieArr = document.cookie.split(';');
    for(var i=0; i < cookieArr.length; i++) {
        var cookieNV = cookieArr[i];
        while (cookieNV.charAt(0) == " ") {
            cookieNV = cookieNV.substring(1, cookieNV.length);
        }
        if (cookieNV.indexOf(nameLookup + "=") == 0) {
            return cookieNV.substring( (nameLookup.length + 1) ,cookieNV.length);
        }
        if (cookieNV.indexOf(nameLookup) == 0) {
            return "";
        }
    }
    return null;
}

function ybFun_EraseCookie(name) {
    ybFun_CreateCookie(name, "", -1);
}
</script>

<script>


ybFindPhNums = ['16105935129', '16105935129', '16105935129', '16105935129', '16105935129'];
ybReplacePhNums = ['18775093463', '14842972338', '14842972450', '18778447906', '16106283841'];

ybFun_ReplaceText();

</script>

<!-- phone insertion script ends -->

</html>
