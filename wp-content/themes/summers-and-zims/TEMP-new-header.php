<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
  <div class="top-bar-left">
    <?php foundationpress_top_bar_r(); ?>

    <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
      <?php get_template_part( 'parts/mobile-top-bar' ); ?>
    <?php endif; ?>

  </div>
</nav>


<div class="years-call-truck">
  <div class="call">
    <span><?php the_field("emergency_heading", "options") ?></span>
    <span><?php the_field("emergency_availability", "options"); ?></span>
    <span><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone", "options"); ?></a></span>
  </div> <!-- .call -->
  <div class="truck">
    <img src="<?php echo $truck_url; ?>" alt="<?php echo $truck_alt; ?>">
  </div> <!-- .truck -->
</div> <!-- .years-call-truck -->

<div class="logos">
  <div class="mascot">
    <a href="<?php the_field('mascot_link', 'options'); ?>"><img src="<?php echo $mascot_url; ?>" alt="<?php echo $mascot_alt; ?>"></a>
  </div>
  <div class="logo">
    <a href="<?php the_field('logo_link', 'options'); ?>" rel="home"><img src="<?php echo $logo_url; ?>" alt="<?php echo $logo_alt; ?>"></a>
  </div>
  <div class="years">
    <span class="years-num"><?php print getYears('1930-01-01');  ?></span>
    <img src="<?php echo $years_url; ?>" alt="<?php echo $years_alt; ?>">
    <span class="years-biz">1930 - <?php echo date('Y'); ?></span>
  </div>
</div> <!-- .logos -->




<div class="time-message">

<?php

// Set timezones and get day and time
date_default_timezone_set('America/New_York');
$currentTime = date('g:i A');
$currentTime24 = date('H:i A');
$currentDay = date('l');

// Set up messages
//$happy = "Happy " . $currentDay . "! ";
$officeOpen = "<p><strong>Happy " . $currentDay . "</strong>! <span class='open'>It's <span id='time'></span> and our office is open.</span></p>";
$officeClosed = "<p><strong>Happy " . $currentDay . "</strong>! <span class='closed'>It's <span id='time'></span> and our main office is closed.</span></p>";

// Set up day/time logic
if($currentDay == "Monday" ||
$currentDay == "Tuesday" ||
$currentDay == "Wednesday" ||
$currentDay == "Thursday" ||
$currentDay == "Friday") {

if($currentTime24 > "07:00 AM" && $currentTime24 < "17:15 PM") {
?>

<?php
echo $officeOpen;
?>

<p><?php the_field("office_open_text", "options"); ?></p>

<?php
} else {
?>

<?php
echo $officeClosed;
?>

<p><?php the_field("office_closed_text", "options"); ?></p>

<?php
}
}

if($currentDay == "Saturday") {

if($currentTime24 >= "08:00 AM" && $currentTime24 <= "12:00 PM") {
?>

<?php
echo $officeOpen;
?>

<p><?php the_field("office_open_text", "options"); ?></p>

<?php
} else {
?>

<?php
echo $officeClosed;
?>

<p><?php the_field("office_closed_text", "options"); ?></p>

<?php
}
}

if($currentDay == "Sunday") {
?>

<?php
echo $officeClosed;
?>

<p><?php the_field("office_closed_text", "options"); ?></p>

<?php
}

?>
</div> <!-- .time-message -->
