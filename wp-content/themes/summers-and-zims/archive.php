<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page" role="main" class="blog general">
	<div class="row">
		<div class="medium-6 large-8 columns">
			<h1>Blog: <?php single_cat_title(); ?></h1>
		    <?php dimox_breadcrumbs(); ?>
<?php 
	if(have_rows("blog_page", 15)):
		while(have_rows("blog_page", 15)): the_row();
			if(get_row_layout() == 'blog_intro'):
?>
          <p><?php the_sub_field('blog_intro'); ?></p>
<?php
			endif;
		endwhile;
	endif;
?>		    
	<?php //do_action( 'foundationpress_before_content' ); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; // End have_posts() check. ?>

		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php } ?>
		</div> <!-- .medium-6 -->
	    <div class="medium-6 large-4 columns">
	      <div class="page-sidebar">
	<?php
	  if(have_rows('sidebar_content', 15)):
	    while(have_rows('sidebar_content', 15)): the_row();
	?>

	<?php
	    if(get_row_layout() == "sidebar_review"):
	?>
	        <div class="box">
	<?php 
	            if(get_sub_field("sidebar_review_title")):
	              echo "<h3>" . get_sub_field("sidebar_review_title") . "</h3>";
	            endif;

	            if(get_sub_field("sidebar_review")):
	              the_sub_field("sidebar_review");
	            endif;

	            if(get_sub_field("sidebar_review_link")):
	              echo "<a href='".get_sub_field("sidebar_review_link")."' class='button blue'>" . get_sub_field("sidebar_review_link_text") . "</a>";
	            endif;            
	?>

	        </div> <!-- .box -->  

	<?php
	    elseif(get_row_layout() == "sidebar_membership"):
	?>
	        <div class="box">
	<?php 
	            if(get_sub_field("sidebar_membership_title")):
	              echo "<h3>" . get_sub_field("sidebar_membership_title") . "</h3>";
	            endif;

	            if(get_sub_field("sidebar_membership_description")):
	              the_sub_field("sidebar_membership_description");
	            endif;

	            if(get_sub_field("sidebar_membership_link")):
	              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
	            endif;            
	?>

	        </div> <!-- .box --> 

	<?php
	    elseif(get_row_layout() == "sidebar_posts"):
	?>
	        <div class="box">
	<?php 
	            if(get_sub_field("sidebar_posts_title")):
	              echo "<h3>" . get_sub_field("sidebar_posts_title") . "</h3>";
	            endif;

	            if(have_rows("sidebar_posts")):
	              while(have_rows("sidebar_posts")): the_row();

	                $posts = get_sub_field('sidebar_post');

	                if($posts):
	?>
	          <article class="service-posts">
	            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	            <?php setup_postdata($post); ?>
	            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	            <footer>
	              Posted on <?php the_date(); ?> in <?php the_category(); ?>
	            </footer>
	            <?php endforeach; ?>
	          </article>
	            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php 
	                endif;                
	              endwhile;
	            endif;

	            if(get_sub_field("sidebar_membership_link")):
	              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
	            endif;            
	?>
	        </div> <!-- .box -->         

	<?php
	    elseif(get_row_layout() == "sidebar_appointment"):
	?>
	        <div class="box">
	<?php 
	            if(get_sub_field("sidebar_appointment_title")):
	              echo "<h3>" . get_sub_field("sidebar_appointment_title") . "</h3>";
	            endif;

	            if(get_sub_field("sidebar_appointment_description")):
	              the_sub_field("sidebar_appointment_description");
	            endif;

	            if(get_sub_field("sidebar_appointment_link")):
	              echo "<a href='".get_sub_field("sidebar_appointment_link")."' class='button blue'>" . get_sub_field("sidebar_appointment_link_text") . "</a>";
	            endif;            
	?>

	        </div> <!-- .box -->                     



	<?php
	    elseif(get_row_layout() == "sidebar_image"):
	?>
	        <div class="box image">
	<?php 
	            if(get_sub_field("sidebar_image_title")):
	              echo "<h3>" . get_sub_field("sidebar_image_title") . "</h3>";
	            endif;

	            if(get_sub_field("sidebar_image_description")):
	              the_sub_field("sidebar_image_description");
	            endif;

	            if(get_sub_field("sidebar_image")):
	              $sidebar_image = get_sub_field("sidebar_image");
	              $sidebar_image_url = $sidebar_image['url'];
	              $sidebar_image_alt = $sidebar_image['alt'];
	            endif;

	            if(get_sub_field("sidebar_image") && get_sub_field("sidebar_image_link")):
	              echo "<a href='".get_sub_field("sidebar_image_link")."'>";
	              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
	              echo "</a>";
	            endif;

	            if(get_sub_field("sidebar_image") && !get_sub_field("sidebar_image_link")):
	              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
	            endif;

	?>

	        </div> <!-- .box -->   

	<?php
	    elseif(get_row_layout() == "sidebar_script"):
	?>
	        <div class="box image">
	<?php 
	            if(get_sub_field("sidebar_script_title")):
	              echo "<h3>" . get_sub_field("sidebar_script_title") . "</h3>";
	            endif;

	            if(get_sub_field("sidebar_script_description")):
	              the_sub_field("sidebar_script_description");
	            endif;

	            if(get_sub_field("sidebar_script")):
	              echo the_sub_field("sidebar_script"); 
	            endif;            
	?>

	        </div> <!-- .box -->        
	<?php
	    endif; // end get_row_layouts
	?>
	       
	<?php
	    endwhile;
	  endif;
	?>
	      </div> <!-- .sidebar -->
	    </div> <!-- medium-6 large-4 sidebar -->
	</div> <!-- .row -->

	

</div> <!-- #page -->

<?php get_footer(); ?>
