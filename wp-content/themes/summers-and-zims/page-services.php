<?php
/**
 * Template Name: Services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="home">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

  <div class="row">
    <div class="medium-12 columns">
      <h1><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </div> <!-- .medium-12 -->
  </div> <!-- .row -->

 <!-- start home page -->
<?php 
  if(have_rows('services_page')):
    while(have_rows('services_page')): the_row();
    


    endwhile;
  endif;
?>
 <!-- end home page -->


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>
 <?php get_sidebar(); ?>

 </div>

 <?php get_footer(); ?>
