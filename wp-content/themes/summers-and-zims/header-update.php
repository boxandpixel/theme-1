<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<?php
	if(is_page('pay-online')):

		//Set Account Values
		$x_login = "WSP-SUMME-2UWVtwArvg";//Payment Page ID
		$transaction_key = "i6JoypTM~ngki3Qumynz";//Transaction Key
		$x_currency_code = "USD";//Currency Code, set to terminal default (usually USD)
		$x_fp_timestamp = time();
		srand(time());
		$x_fp_sequence = rand(1000, 100000) + 123456;

		//Generate Hash
		function createHash($x_amount,$x_login,$transaction_key,$x_currency_code) {
			global $transaction_key, $x_fp_timestamp, $x_login, $x_fp_sequence, $x_amount,$x_currency_code;
			$hmac_data = $x_login . "^" . $x_fp_sequence . "^" . $x_fp_timestamp . "^" . $x_amount . "^" . $x_currency_code;
			$x_fp_hash = hash_hmac('md5', $hmac_data, $transaction_key);
			return $x_fp_hash;
		}
	endif;
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- start icons -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-site-verification" content="WiWOJVp2oBOlsbVMarag14F0dg8XCEOm9Ja497jlvn0" />
		<!-- end icons -->
<?php
	if(is_page('pay-online')):
?>
		<script type="text/javascript">
		function forward()
		{
			var identifier = '<?php echo $_REQUEST["identifier"]; ?>';
			if(identifier)
			{
			//Comment if using a Demo account
			document.redirectForm.action="https://checkout.globalgatewaye4.firstdata.com/payment";
			//Uncomment if using a Demo account
			//document.redirectForm.action="https://demo.globalgatewaye4.firstdata.com/payment";
			document.redirectForm.submit();
			}
		}
		alert(identifier);
		</script>
<?php
	endif;
?>
		
		<?php wp_head(); ?>
		<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0077/2416.js" async="async"></script>
	</head>
	<body <?php body_class(); ?> <?php if(is_page('pay-online')):?>onLoad="forward()"<?php endif;?>>
		<div style="width: 0; height: 0;">
			<?php include("img/img.svg"); ?>
		</div> <!-- .svg-holder -->
<?php
	global $woocommerce;
?>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

<?php
	$mascot = get_field("mascot", "options");
	$mascot_url = $mascot['url'];
	$mascot_alt = $mascot['alt'];

	$logo = get_field("logo", "options");
	$logo_url = $logo['url'];
	$logo_alt = $logo['alt'];

	$logo_small = get_field("logo_small", "options");
	$logo_small_url = $logo_small['url'];
	$logo_small_alt = $logo_small['alt'];

	$years = get_field("years_of_experience", "options");
	$years_url = $years['url'];
	$years_alt = $years['alt'];

	$truck = get_field("truck", "options");
	$truck_url = $truck['url'];
	$truck_alt = $truck['alt'];

	// Get Years in Business
	function getYears($then) {
	    $then_ts = strtotime($then);
	    $then_year = date('Y', $then_ts);
	    $years = date('Y') - $then_year;
	    if(strtotime('+' . $years . ' years', $then_ts) > time()) $years--;
	    return $years;
	}

?>

	<div data-sticky-container>
	<div data-sticky style="width: 100%;" data-options="marginTop:0; stickyOn:small">
	<header id="masthead" class="site-header <?php
if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) : ?>cart-active<?php endif; ?>" role="banner">

		<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
			<div class="nav-flex">
				<div class="logo-small">
					<a href="<?php the_field('logo_link', 'options'); ?>" rel="home"><img src="<?php echo $logo_small_url; ?>" alt="<?php echo $logo_small_alt; ?>"></a>
				</div>
			  <div class="top-bar-left">
			    <?php foundationpress_top_bar_r(); ?>

			    <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
			      <?php get_template_part( 'parts/mobile-top-bar' ); ?>
			    <?php endif; ?>

			  </div>
			</div>

		</nav>
		<div class="header-items">

			<section class="logo-years">
				<div class="header-logo">
					<a href="<?php the_field('logo_link', 'options'); ?>" rel="home"><img src="<?php echo $logo_url; ?>" alt="<?php echo $logo_alt; ?>"></a>
				</div>

				<div class="header-years">
					<div class="years">
					    <span class="years-num"><?php print getYears('1930-01-01');  ?></span>
					    <img src="<?php echo $years_url; ?>" alt="<?php echo $years_alt; ?>">
					    <span class="years-biz">1930 - <?php echo date('Y'); ?></span>
				  </div>
				</div>
			</section>

			<div class="header-call">
				<div class="time-message">

				<?php

				// Set timezones and get day and time
				date_default_timezone_set('America/New_York');
				$currentTime = date('g:i A');
				$currentTime24 = date('H:i A');
				$currentDay = date('l');

				// Set up messages
				//$happy = "Happy " . $currentDay . "! ";
				$officeOpen = "<p><span class='open'><strong>It's " . $currentDay . "</strong> at <span id='time'></span> and our office is open. </span></p>";
				$officeClosed = "<p><span class='closed'><strong>It's " . $currentDay . "</strong> at <span id='time'></span> and our on-call staff will be happy to assist you. </span></p>";

				// Set up day/time logic
				if($currentDay == "Monday" ||
				$currentDay == "Tuesday" ||
				$currentDay == "Wednesday" ||
				$currentDay == "Thursday" ||
				$currentDay == "Friday") {

				if($currentTime24 > "07:00 AM" && $currentTime24 < "17:15 PM") {
				?>

				<?php
				echo $officeOpen;
				?>

				<p><?php the_field("office_open_text", "options"); ?></p>

				<?php
				} else {
				?>

				<?php
				echo $officeClosed;
				?>

				<p><?php the_field("office_closed_text", "options"); ?></p>

				<?php
				}
				}

				if($currentDay == "Saturday") {

				if($currentTime24 >= "08:00 AM" && $currentTime24 <= "12:00 PM") {
				?>

				<?php
				echo $officeOpen;
				?>

				<p><?php the_field("office_open_text", "options"); ?></p>

				<?php
				} else {
				?>

				<?php
				echo $officeClosed;
				?>

				<p><?php the_field("office_closed_text", "options"); ?></p>

				<?php
				}
				}

				if($currentDay == "Sunday") {
				?>

				<?php
				echo $officeClosed;
				?>

				<p><?php the_field("office_closed_text", "options"); ?></p>

				<?php
				}

				?>
				</div> <!-- .time-message -->
		    
		    <span class="call-message"><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone_message", "options"); ?></a></span>
		    <span class="call-number"><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone_number", "options"); ?></a></span>
			</div>

			<div class="header-mascot">
				<a href="<?php the_field('mascot_link', 'options'); ?>"><img src="<?php echo $mascot_url; ?>" alt="<?php echo $mascot_alt; ?>"></a>
			</div> <!-- .header-mascot -->

		</div> <!-- .header-top-rows -->




		<!-- Mobile nav menu icon -->
		<div class="title-bar" data-responsive-toggle="site-navigation">
			<div class="title-bar-brand">
				<div class="logo-small">
					<a href="<?php the_field('logo_link', 'options'); ?>" rel="home"><img src="<?php echo $logo_small_url; ?>" alt="<?php echo $logo_small_alt; ?>"></a>
				</div>
				<span class="title-bar-call"><a href="<?php the_field('phone_link', 'options') ?>"><?php the_field("phone_number", "options"); ?></a></span>
			</div>
			<div class="title-bar-menu">
		  		<button class="menu-icon float-right" type="button" data-toggle="offCanvas"></button>
			  	<div class="title-bar-title">
			    	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		  		</div>
			</div>
		</div>

	</header>
	</div>
	</div>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' ); ?>
