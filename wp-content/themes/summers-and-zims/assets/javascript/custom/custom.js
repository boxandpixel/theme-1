
// Toggle function
$.fn.clicktoggle = function(a, b) {
    return this.each(function() {
        var clicked = false;
        $(this).click(function() {
            if (clicked) {
                clicked = false;
                return b.apply(this, arguments);
            }
            clicked = true;
            return a.apply(this, arguments);
        });
    });
};

// jQuery Functions

$(document).ready(function() {


    $(".link-group a[href^=#]").on("click", function() {
      // Remove hashtags in url
    });

    $(".slides-case").slick({
        cssEase: "ease-in-out",
        speed: 300,
        dots: true,
        lazyLoad: 'ondemand'
    });

    $(".section-slides").slick({
        cssEase: "ease-in-out",
        speed: 300,
        dots: false,
        lazyLoad: 'ondemand'
    });

    $(".slides-arrow").click(function() {
        $(this).toggleClass("rotate");
        $(".slides-content").toggle();
    });

    $(".home-diff").show();


    $(".staff-members").slick({
        cssEase: "ease-in-out",
        speed: 300,
        dots: false,
		slidesToShow: 8,
		slidesToScroll: 8,
        lazyLoad: 'progressive',
        draggable: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 8,
					slidesToScroll: 8,
					infinite: true,
				}
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					infinite: true,
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
				}
			},	
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
				}
			},				


		]

    });

    // $(".service-slides").slick({
	   //  cssEase: "ease-in-out",
	   //  speed: 300,
	   //  dots: true,
	   //  lazyLoad: 'progressive',
	   //  draggable: true,
    // }); 

    //$("input[type='tel']").mask('(000) 000-0000');

    // Time function for header

    // Add zero to hour/minute/second if less than 10
	function addZero(i) {
	    if (i < 10) {
	        i = "0" + i;
	    }
	    return i;
	}

	// Get and display time
	function getTime() {
	    var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();

	    // add a zero in front of numbers<10
	    m = addZero(m);

	    // get am/pm
	    ampm = h >= 12 ? 'PM' : "AM";

	    // convert to 12 hour am/pm format
	    h = h % 12;
	    h = h ? h : 12;

	    var time = document.getElementById('time').innerHTML = h + ":" + m + " " + ampm;


	    t = setTimeout(function () {
	        getTime();
	    }, 500);


	}
	getTime();

	// Header Scroll Events
	$(window).scroll(function() {
		if($(window).scrollTop() > 230) {
			$(".header-items").addClass("scroll");
			$(".header-mascot").addClass("scroll");

			$("nav#site-navigation").addClass("scroll");
			$(".logo-small").css("opacity","1");
			$(".title-bar-call").css("opacity","1");

			$(".header-time").addClass("scroll");

			$(".title-bar").addClass("scroll");

			$(".call-message").addClass("scroll");
			$("header#masthead").addClass("scroll");

			} else {
			$("nav#site-navigation").removeClass("scroll");
			$(".header-items").removeClass("scroll");
			$(".header-mascot").removeClass("scroll");

			$(".header-time").removeClass("scroll");
			$(".logo-small").css("opacity","0");
			$(".title-bar-call").css("opacity","0");
			$(".title-bar").removeClass("scroll");
			$(".call-message").removeClass("scroll");
			$("header#masthead").removeClass("scroll");
		}

		// Header Update

		if($(window).scrollTop() > 100) {
			$("header#update").addClass("scroll");
			$(".header__years-of-experience").addClass("scroll");
			$(".header__time-message").addClass("scroll");
			$(".header__left").find("img").addClass("scroll");
		}

		else {
			$("header#update").removeClass("scroll");
			$(".header__years-of-experience").removeClass("scroll");
			$(".header__time-message").removeClass("scroll");
			$(".header__left").find("img").removeClass("scroll");
		}
	});

	$(".acc").find("h4").click(function() {
		$(this).toggleClass("active");
		$(this).next().toggleClass("show");
		// $(".content").not($(this).next()).hide();
	});


	// Home Update The Difference Show/Hide

	$(".difference__paginate-1").click(function() {
		$(".difference__featured-1").css("display","flex");

		$(".difference__featured-2").hide();
		$(".difference__featured-3").hide();
		$(".difference__featured-4").hide();
	});

	$(".difference__paginate-2").click(function() {
		$(".difference__featured-2").css("display","flex");

		$(".difference__featured-1").hide();
		$(".difference__featured-3").hide();
		$(".difference__featured-4").hide();
	});

	$(".difference__paginate-3").click(function() {
		$(".difference__featured-3").css("display","flex");

		$(".difference__featured-1").hide();
		$(".difference__featured-2").hide();
		$(".difference__featured-4").hide();
	});

	$(".difference__paginate-4").click(function() {
		$(".difference__featured-4").css("display","flex");

		$(".difference__featured-1").hide();
		$(".difference__featured-2").hide();
		$(".difference__featured-3").hide();
	});


});
