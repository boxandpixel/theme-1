<?php
/**
 * 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page" role="main" class="blog general">

  <div class="row">
	<div class="medium-6 large-8 columns">
		<h1><?php single_post_title(); ?></h1>
<?php 
	if(have_rows("blog_page", 15)):
		while(have_rows("blog_page", 15)): the_row();
			if(get_row_layout() == 'blog_intro'):
?>
          <p><?php the_sub_field('blog_intro'); ?></p>
<?php
			endif;
		endwhile;
	endif;
?>
	<?php if ( have_posts() ) : ?>

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; // End have_posts() check. ?>

		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php } ?>

	</div> <!-- .medium-6 large-8 -->
    <div class="medium-6 large-4 columns">
      <div class="page-sidebar">
<?php
  if(have_rows('sidebar_content', 15)):
    while(have_rows('sidebar_content', 15)): the_row();
?>

<?php
    if(get_row_layout() == "sidebar_review"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_review_title")):
              echo "<h3>" . get_sub_field("sidebar_review_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_review")):
              the_sub_field("sidebar_review");
            endif;

            if(get_sub_field("sidebar_review_link")):
              echo "<a href='".get_sub_field("sidebar_review_link")."' class='button blue'>" . get_sub_field("sidebar_review_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->  

<?php
    elseif(get_row_layout() == "sidebar_membership"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_membership_title")):
              echo "<h3>" . get_sub_field("sidebar_membership_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_membership_description")):
              the_sub_field("sidebar_membership_description");
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box --> 

<?php
    elseif(get_row_layout() == "sidebar_posts"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_posts_title")):
              echo "<h3>" . get_sub_field("sidebar_posts_title") . "</h3>";
            endif;

            if(have_rows("sidebar_posts")):
              while(have_rows("sidebar_posts")): the_row();

                $posts = get_sub_field('sidebar_post');

                if($posts):
?>
          <article class="service-posts">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <footer>
              Posted on <?php the_date(); ?> in <?php the_category(); ?>
            </footer>
            <?php endforeach; ?>
          </article>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php 
                endif;                
              endwhile;
            endif;

            if(get_sub_field("sidebar_membership_link")):
              echo "<a href='".get_sub_field("sidebar_membership_link")."' class='button blue'>" . get_sub_field("sidebar_membership_link_text") . "</a>";
            endif;            
?>
        </div> <!-- .box -->         

<?php
    elseif(get_row_layout() == "sidebar_appointment"):
?>
        <div class="box">
<?php 
            if(get_sub_field("sidebar_appointment_title")):
              echo "<h3>" . get_sub_field("sidebar_appointment_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_appointment_description")):
              the_sub_field("sidebar_appointment_description");
            endif;

            if(get_sub_field("sidebar_appointment_link")):
              echo "<a href='".get_sub_field("sidebar_appointment_link")."' class='button blue'>" . get_sub_field("sidebar_appointment_link_text") . "</a>";
            endif;            
?>

        </div> <!-- .box -->                     



<?php
    elseif(get_row_layout() == "sidebar_image"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_image_title")):
              echo "<h3>" . get_sub_field("sidebar_image_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_image_description")):
              the_sub_field("sidebar_image_description");
            endif;

            if(get_sub_field("sidebar_image")):
              $sidebar_image = get_sub_field("sidebar_image");
              $sidebar_image_url = $sidebar_image['url'];
              $sidebar_image_alt = $sidebar_image['alt'];
            endif;

            if(get_sub_field("sidebar_image") && get_sub_field("sidebar_image_link")):
              echo "<a href='".get_sub_field("sidebar_image_link")."'>";
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
              echo "</a>";
            endif;

            if(get_sub_field("sidebar_image") && !get_sub_field("sidebar_image_link")):
              echo "<img src='".get_sub_field("sidebar_image")['url']."' alt='".get_sub_field("sidebar_image")['alt']."'>";
            endif;
// Begin Image Button
            if(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "External Link"):
?>
              <a href="<?php the_sub_field('sidebar_image_button_external_link'); ?>" target="_blank" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            elseif(get_sub_field("sidebar_image_button_text") && get_sub_field("sidebar_image_button_type") == "Internal Link"):
?>              
              <a href="<?php the_sub_field('sidebar_image_button_page_link'); ?>" class="button blue"><?php the_sub_field('sidebar_image_button_text'); ?></a>
<?php
            endif;
// End Image Button            
?>

        </div> <!-- .box -->   

<?php
    elseif(get_row_layout() == "sidebar_script"):
?>
        <div class="box image">
<?php 
            if(get_sub_field("sidebar_script_title")):
              echo "<h3>" . get_sub_field("sidebar_script_title") . "</h3>";
            endif;

            if(get_sub_field("sidebar_script_description")):
              the_sub_field("sidebar_script_description");
            endif;

            if(get_sub_field("sidebar_script")):
              echo the_sub_field("sidebar_script"); 
            endif;            
?>

        </div> <!-- .box -->        
<?php
    endif; // end get_row_layouts
?>
       
<?php
    endwhile;
  endif;
?>
      </div> <!-- .sidebar -->
    </div> <!-- medium-6 large-4 sidebar -->
  </div> <!-- .row -->

	<?php //get_sidebar(); ?>


</div> <!-- #page -->

<?php get_footer(); ?>
