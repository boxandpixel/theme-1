<?php

// Register Staff
function register_staff() {
	register_post_type('staff', array(
		'label' => 'Staff',
		'menu_icon' => 'dashicons-admin-users',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => false,
		'supports' => array('title'),
		'labels' => array (
			'name' => 'Staff',
			'singular_name' => 'Staff',
			'menu_name' => 'Staff',
			'add_new' => 'Add Staff',
			'add_new_item' => 'Add New Staff',
			'edit' => 'Edit',
			'edit_item' => 'Edit Staff',
			'new_item' => 'New Staff',
			'view' => 'View',
			'view_item' => 'View Staff',
			'search_items' => 'Search Staff',
			'not_found' => 'No Staff Found',
			'not_found_in_trash' => 'No Staff Found in Trash',
			'parent' => 'Parent Staff'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_staff');



// Register Testimonials
function register_reviews() {
	register_post_type('reviews', array(
		'label' => 'Reviews',
		'menu_icon' => 'dashicons-admin-users',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => false,
		'supports' => array('title'),
		'labels' => array (
			'name' => 'Reviews',
			'singular_name' => 'Review',
			'menu_name' => 'Reviews',
			'add_new' => 'Add Review',
			'add_new_item' => 'Add New Review',
			'edit' => 'Edit',
			'edit_item' => 'Edit Review',
			'new_item' => 'New Review',
			'view' => 'View',
			'view_item' => 'View Reviews',
			'search_items' => 'Search Reviews',
			'not_found' => 'No Reviews Found',
			'not_found_in_trash' => 'No Reviews Found in Trash',
			'parent' => 'Parent Review'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_reviews');

?>
