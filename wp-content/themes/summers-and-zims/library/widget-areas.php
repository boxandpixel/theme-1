<?php
/**
 * Register widget areas
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */



if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
function foundationpress_sidebar_widgets() {
	
	register_sidebar(array(
	  'id' => 'sidebar-widgets',
	  'name' => __( 'Sidebar widgets', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="small-12 columns">',
	  'after_widget' => '</div></article>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));
	
	/*
	register_sidebar(array(
	  'id' => 'footer-copyright',
	  'name' => __( 'Footer Copyright', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-address',
	  'name' => __( 'Footer Address', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));

	register_sidebar(array(
	  'id' => 'footer-contact',
	  'name' => __( 'Footer Contact', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-certifications',
	  'name' => __( 'Footer Certifications', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-angies',
	  'name' => __( 'Footer Angies List', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-social',
	  'name' => __( 'Footer Social Icons', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s social">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-text',
	  'name' => __( 'Footer Text', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-map',
	  'name' => __( 'Footer Map', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s"><div id="footer-map">',
	  'after_widget' => '</div></div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));	

	register_sidebar(array(
	  'id' => 'footer-login',
	  'name' => __( 'Footer Login', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<div id="%1$s" class="section widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));							

	*/
}

add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;



?>
