<?php get_header(); ?>
			<div id="page" class="staff-detail">

				<div class="row">
					<div class="medium-12 columns">
						<h1><?php the_field("name"); ?></h1>
						<?php dimox_breadcrumbs(); ?>      
						
					</div> <!-- .medium-12 -->
				</div> <!-- .row -->

				<div class="row">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="small-12 medium-6 columns">
				<?php
					$image = get_field("large_image");
					$image_url = $image['url'];
					$image_alt = $image['alt'];
				?>
				<?php
					if(!empty($image)):
				?>
						<img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
				<?php
					endif;
				?>
					</div> <!-- .small-12 -->
					<div class="small-12 medium-6 columns">
				<?php 
					$staff_review = get_field("staff_review");
					$job_title = get_field("job_title");
					$start_date = get_field("start_date");
					$experience = get_field("experience_start_date");
					$certifications = get_field("certifications", false, false);
					$favorite = get_field("favorite_part", false, false);
					$hobbies = get_field("hobbies", false, false);
					$family = get_field("family", false, false);
				?>
				<?php
					if(!empty($staff_review)):
				?>
						<div class="section staff-review">
							<?php echo $staff_review; ?>
						</div> <!-- .section -->								
				<?php
					endif;
					if(!empty($job_title)):
				?>
						<div class="section">
							<h4>Job Title</h4>
							<p><?php echo $job_title; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
					if(!empty($start_date)):
				?>
						<div class="section">
							<h4>Working at Summers &amp; Zim's Since</h4>
							<p><?php echo $start_date; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
					if(!empty($experience)):
					
					$today = new DateTime("now");
					$start = new DateTime($experience);				
					$interval = $today->diff($start);
						if($interval->y < 1):
							$timeframe = $interval->m . " months";

						elseif($interval->y == 1):
							$timeframe = $interval->y . " year";

						else:
							$timeframe = $interval->y . " years";

						endif;
				
				?>
						<div class="section">
							<h4>Career Experience</h4>
							<p><?php echo $timeframe; ?></p>
						</div> <!-- .section -->						
				<?php
					endif;
					if(!empty($certifications)):
				?>
						<div class="section">
							<h4>Certifications</h4>
							<p><?php echo $certifications; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
					if(!empty($favorite)):
				?>
						<div class="section">
							<h4>Favorite Part of Working at Summers &amp; Zim's</h4>
							<p><?php echo $favorite; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
					if(!empty($hobbies)):
				?>
						<div class="section">
							<h4>Hobbies</h4>
							<p><?php echo $hobbies; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
					if(!empty($family)):
				?>
						<div class="section">
							<h4>Family</h4>
							<p><?php echo $family; ?></p>
						</div> <!-- .section -->
				<?php
					endif;
				?>												
					<!-- hello? -->
					</div> <!-- .small-12 -->	
				<?php endwhile;?>

				</div> <!-- .row -->

				<div class="row">

					<div class="small-12 columns view-all-staff">
						<a href="/staff" class="button blue">View All Staff</a>
					</div> <!-- .columns -->
									
				</div> <!-- .row -->
			</div> <!-- #page -->
 <?php get_footer(); ?>			


