<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'parts/featured-image' ); ?>

 <div id="page" role="main" class="login">

 <?php do_action( 'foundationpress_before_content' ); ?>
  <div class="row">
      <div class="small-12 medium-8 columns end">
 <?php while ( have_posts() ) : the_post(); ?>
    
    <h1 class="entry-title"><?php the_title(); ?></h1>

           <?php wp_login_form(); ?>

 <?php endwhile;?>
      </div> <!-- .small-12 -->
    </div> <!-- .row -->
 </div> <!-- #page -->

 <?php get_footer(); ?>
