		

		<?php

		// Set timezones and get day and time
		date_default_timezone_set('America/New_York');
		$currentTime = date('g:i A');
		$currentTime24 = date('H:i A');
		$currentDay = date('l');

		// Set up messages
		$officeOpen = "<span class='open'><strong>It's " . $currentDay . "</strong> at <span id='time'></span> and our office is open. </span>";
		$officeClosed = "<span class='closed'><strong>It's " . $currentDay . "</strong> at <span id='time'></span> and our on-call staff will be happy to assist you. </span>";

		// Set up day/time logic
		if($currentDay == "Monday" ||
		$currentDay == "Tuesday" ||
		$currentDay == "Wednesday" ||
		$currentDay == "Thursday" ||
		$currentDay == "Friday") {

		if($currentTime24 > "07:00 AM" && $currentTime24 < "17:15 PM") {
		?>

		<?php
		echo $officeOpen;
		?>

		<!-- <p><?php the_field("office_open_text", "options"); ?></p> -->

		<?php
		} else {
		?>

		<?php
		echo $officeClosed;
		?>

		<!-- <p><?php the_field("office_closed_text", "options"); ?></p> -->

		<?php
		}
		}

		if($currentDay == "Saturday") {

		if($currentTime24 >= "08:00 AM" && $currentTime24 <= "12:00 PM") {
		?>

		<?php
		echo $officeOpen;
		?>

		<!-- <p><?php the_field("office_open_text", "options"); ?></p> -->

		<?php
		} else {
		?>

		<?php
		echo $officeClosed;
		?>

		<!-- <p><?php the_field("office_closed_text", "options"); ?></p> -->

		<?php
		}
		}

		if($currentDay == "Sunday") {
		?>

		<?php
		echo $officeClosed;
		?>

		<!-- <p><?php the_field("office_closed_text", "options"); ?></p> -->

		<?php
		}

		?>
	