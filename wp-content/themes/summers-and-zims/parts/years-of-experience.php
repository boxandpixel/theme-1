		<?php
			$years = get_field("years_of_experience", "options");
			$years_url = $years['url'];
			$years_alt = $years['alt'];

			// Get Years in Business
			function getYears($then) {
			    $then_ts = strtotime($then);
			    $then_year = date('Y', $then_ts);
			    $years = date('Y') - $then_year;
			    if(strtotime('+' . $years . ' years', $then_ts) > time()) $years--;
			    return $years;
			}
		?>