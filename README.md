# Brian Gunzenhauser

This repository includes example Wordpress theme files. 

## Getting Started

All theme files can be reviewed by navigating to /wp-content/themes/summers-and-zims in this repository.

## Built With

* [FoundationPress](https://github.com/olefredrik/foundationpress) - This site was built on top of the FoundationPress responsive starter framework. 
* [Slick](http://kenwheeler.github.io/slick/) - Slick Slider
* [Fancybox](https://github.com/fancyapps/fancybox) - A jQuery lightbox script

## Authors

* **Brian Gunzenhauser**
